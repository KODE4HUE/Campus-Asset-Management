<?php

use App\Services\Staff\StaffServiceManager;

/**
 * Description of StaffController
 *
 * @author hubert
 */
class StaffController extends BaseController
{
    
    protected $restful = true;
    protected $staffServiceManager;
    
    public function __construct(StaffServiceManager $staffServiceManager) {
        $this->staffServiceManager = $staffServiceManager;
    }
    
    public function getAllStaff()
    {
         $statusCode = 200;
         
         $this->staffServiceManager->getAllStaff();
         
         $result = $this->staffServiceManager->getResult();
         
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            
            if($result['error-messages']['validation-messsages']){
                $statusCode = 400;
            }
            else
            {
                $statusCode = 500;
            }
            
        }
        
         return Response::json($result, $statusCode);
        
    }
    
    public function getStaff($id)
    {
        $statusCode = 200;
        
        $result = $this->staffServiceManager->getStaff($id);
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 401;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }
    
    /*
     * Staff controller method to retrieve the currently loggedd in staff
     */
    public function getLoggedInStaff()
    {
        $statusCode = 200;
        
        $this->staffServiceManager->getLoggedInStaff();
        
        $result =  $this->staffServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
            
        }
        else
        {
            if($result['error-messages']['validation-messsages'])
            {
                $statusCode = 401;
            }
            elseif($result['error']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
            
        }
        
         return Response::json($result, $statusCode);
    }
    
    public function createStaff()
    {
       
        $statusCode = 200;
        
        $input = Input::only(
                'staff_number',
                'first_name', 'last_name', 'email', 'gender_id',
                'job_description', 'cell_phone_number',
                'profile_image',
                'address_line',
                'is_user',
                'password',
                'confirm_password',
                'has_role',
                'role_id' /***** Array of roles to add to user **************/
                
        );
        
        if(isset($input['role_id']))
        {
            
            if(!empty($input['role_id']))
            {
                 $input['has_role'] = true;
            }
            else{
                 $input['has_role'] = false;
            }
               
        }
        else{
            $input['has_role'] = false;
        }
       

        if(isset($input['is_user']))
        {
            if($input['is_user'] == '1')
            {
                $input['is_user'] = true;
            }
            else{
                 $input['is_user'] = false;
            }
        }
        else{
            $input['is_user'] = false;
        }
        
        if(!isset($input['role_id']))
        {
            $input['role_id'] = false;
        }
        
        $this->staffServiceManager->createStaff($input);
        
        $result  = $this->staffServiceManager->getResult();
        
        if(!$result['error'])
        {
              $statusCode = 200;
        }
        else
        {
            
            
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 400;
            }
            else
            {
                $statusCode = 500;
            }
        }
            
        return Response::json($result, $statusCode);
    }
    
    public function updateStaff($staffId)
    {
        
        $statusCode = 200;
        
        $input = Input::only(
                'staff_number',
                'first_name', 'last_name', 'email', 'gender_id',
                'job_description', 'cell_phone_number', 'address_line',
                'is_user',
                'user_id'
        );
        
        $input['staff_id'] = $staffId;
        
        $this->staffServiceManager->updateStaff($input);
        
        $result = $this->staffServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }
    
    public function archiveStaff($staffId)
    {
        
        $statusCode = 200;
        
        $input = [
            'staff_id' => $staffId
        ];
        
        $this->staffServiceManager->archiveStaff($input);
        
        $result = $this->staffServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        
        return Response::json($result, $statusCode);
        
    }
    
    
    /**
     * Route to activate a new staff's account so
     * they can login to website
     * @params $confirmationCode
     */
    public function confirmStaffAccount($confirmationCode)
    {
        $statusCode = 200;
        
        $this->staffServiceManager->confirmStaffAccount($confirmationCode);
        
        $result = $this->staffServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
                
    }
}
