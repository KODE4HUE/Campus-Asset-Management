<?php namespace App\Repositories\System\Gender;

use App\Repositories\BaseRepository;
use Model\System\Gender;

/**
 * Description of GenderRepository
 *
 * @author hue
 */
class GenderRepository extends BaseRepository implements IGenderRepository {

    protected $gender;
    
    public function __construct(Gender $gender, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->gender = $gender;
    }
}
