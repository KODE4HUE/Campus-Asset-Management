Hello, {{$full_name}}

Please login to your staff account on Campus Asset Management system via our website using the following link:

{{$link}}

<br /> <br />

Or you can login via our Android Application provided via Application Support.

Your initial login credentials are: <br /> <br />

Username/Employee ID: {{$staff_number }} <br />
Password: {{$password }} <br />  <br />

Please ensure you reset your password on your first login to UCAMS via our website or mobile application.


