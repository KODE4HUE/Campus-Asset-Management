<?php namespace App\Repositories\Asset\AssetModel;

use App\Repositories\BaseRepository;
use Model\Asset\AssetModel;

/**
 * Description of AssetModelRepository
 *
 * @author hue
 */
class AssetModelRepository extends BaseRepository implements IAssetModelRepository 
{

    
    
    public function __construct(AssetModel $assetModel, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage=25)
    {
        parent::__construct($assetModel, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
