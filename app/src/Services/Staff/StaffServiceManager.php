<?php namespace App\Services\Staff;

use App\Repositories\Staff\IStaffRepository;
use App\Repositories\System\Authentication\IAuthenticationRepository;
use App\Repositories\System\UserManagement\User\IUserRepository;
use App\Repositories\System\UserManagement\UserRole\IUserRoleRepository;
use App\Services\ServiceManager;
use App\Services\Staff\Validation\ConfirmStaffValidation;
use App\Services\Staff\Validation\CreateStaffValidation;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Description of StaffServiceManager
 *
 * @author hubert
 */
class StaffServiceManager extends ServiceManager
{
    //put your code here
    
    protected $createStaffValidation;
    protected $confirmStaffValidation;
    protected $staffRepository;
    protected $userRepository;
    protected $userRoleRepository;
    protected $authenticationRepository;


    protected $userRoles = false;


    /*
     * flags
     */
    protected $hasRole = false; // determines if there is a roles array for when the staff is being created

    public function __construct( 
        IStaffRepository  $staffRepository,
        IUserRepository $userRepository,
        IUserRoleRepository $userRoleRepository,
        IAuthenticationRepository $authenticationRepository,
        CreateStaffValidation $createStaffValidation,
        ConfirmStaffValidation $confirmStaffValidation
    ) 
    {
        
        $this->staffRepository = $staffRepository;
        $this->userRepository = $userRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->authenticationRepository = $authenticationRepository;
        $this->createStaffValidation = $createStaffValidation;
        $this->confirmStaffValidation = $confirmStaffValidation;
       
    }
    
    public function createStaff($data)
    {
        
        $created = false;
        
        try
        {
            

            
            // get currently logged in user information
            $currentlyLoggedInUserResult = $this->authenticationRepository->getLoggedInUser();
            
            if($currentlyLoggedInUserResult['error'])
            {
                throw new Exception('Could not retrieve currently logged in user');
            }
            
            $currentlyLoggedInUser = $currentlyLoggedInUserResult['data'];
            
            /*
             * Sets profile image to empty string if the profile image file is
             * not set or empty
             */
            if(isset($data['profile_image']))
            {
                if(empty($data['profile_image']))
                {
                   $data['profile_image'] = ""; 
                }
                else{
                    $data['profile_image'] = "";
                }
            }
            else
            {
                $data['profile_image'] = "";
            }

            if($data['is_user'] == true)
            {
                // this staff has a user account


                $this->createStaffValidation->setIsUser();

                if($this->createStaffValidation->passes($data, $data['has_role']))
                {

                    //begin to save staff and user

                     /*
                     * Removes the is_user attribute from data array to prevent insert into
                     * Database
                     */
                    $this->removeIsUserFromData($data);
                    
                    $this->setHasRoleFlag($data['has_role']);
                    
                    $this->removeHasRoleFromArray($data);
                    
                    $this->setUserRoles($data['role_id']);
                    
                    $this->removeRoleArrayFromData($data);
                    /*
                     * Begin transaction
                     */
                    $this->staffRepository->beginTransaction();


                    /*
                     * Take out staff information from user information
                     */
                    $staffData = [
                        'staff_number'          => $data['staff_number'],
                        'first_name'            => $data['first_name'],
                        'last_name'             => $data['last_name'],
                        'gender_id'             => $data['gender_id'],
                        'job_description'       => $data['job_description'],
                        'cell_phone_number'     => $data['cell_phone_number'],
                        'profile_image'         => $data['profile_image'],
                        'email'                 => $data['email'],
                        'created_by_user_id'    => $currentlyLoggedInUser['id']
                    ];

                    /*
                     * Set random password code
                     */
                    $password = $this->generateRandomString(16);

                   // $confirmationCode = str_random(60);

                    $userData = [
                        'username'              => $data['staff_number'],
                        'email'                 => $data['email'],
                        'password'              => Hash::make($password),
                        'confirmed'             => 1,
                        'first_login'           => 1    
                    ];

                    /*
                     * unset initial data passed in service
                     */    
                    unset($data);
                    /*
                     * create staff
                     */
                    $this->result = $this->staffRepository->create($staffData);

                    if($this->result['error'])
                    {
                        throw new Exception("There was an error while saving the staff data for user, while trying to create a new save");
                    }


                    /*
                     * Grab id for newly created staff
                     */
                    $newStaffId = $this->result['data']['id'];

                    /*
                     * Reset errors result
                     */
                    $this->resetResult();


                    /**
                     * Set staff id  for user to be created
                     */
                    $userData['staff_id'] = $newStaffId;

                    /*
                     * create user
                     */
                    $this->result = $this->userRepository->create($userData);
                    

                    if($this->result['error'])
                    {
                        throw new Exception("There was an error while trying to save user data while creating staff");
                    }
                    
                    $newUserId = $this->result['data']['id'];
                    
                    $this->resetResult(); // reset error result
                    
                    if($this->hasRole)
                    {

                        // then add the roles to the user
                        foreach($this->userRoles as $key => $value )
                        {
                            $newUserRole = [
                                'role_id' => $value,
                                'user_id' => $newUserId
                            ];
                            
                            $this->result = $this->userRoleRepository->create($newUserRole);
                            
                            if($this->result['error'])
                            {
                                throw new Exception("There was an error while while adding the staff role(s)");
                            }
                            
                            /*
                            * Reset errors result
                            */
                           $this->resetResult();
                        }
                        
                    }
                    
                    $full_name = $staffData['first_name'] . ' ' . $staffData['last_name'];
                    /*
                     * Send activation email for staff to 
                     * register
                     */                
                    Mail::send(
                            'emails.auth.activation',        
                            ['link' => 'ucams/login', 'password' => $password , 'staff_number' => $staffData['staff_number'], 'full_name' => $full_name],  
                    function($message) use ($userData){
                        $message->to($userData['email']);
                    } );
                    
                    /*
                     * Set success message
                     */
                    $this->result['data']['sucess-message'] = "Successfully created staff and sent confirmation email to the user's account";

                    
                     /*
                    * Rollback transaction if a transaction was started
                    */
                    if($this->staffRepository->getTransactionLevel() > 0)
                    {
                        /*
                         * Commit transaction
                         */
                        $this->staffRepository->commitTransaction();
                    }

                    

                    $created = true;

                }
                else
                {
                    $created =false;

                    $this->result['data'] = false;
                    $this->result['error'] = true;
                    $this->result['error-messages']['validation-messages'] = $this->createStaffValidation->getErrors();
                    
                }
            }
            else
            {
                //the staff does not have a user account

                if($this->createStaffValidation->passes($data))
                {
                    //begin to save staff

                    /*
                     * Removes the is_user attribute from data array to prevent 
                     * insert into Database
                     */
                    $this->removeIsUserFromData($data);


                    $this->result = $this->staffRepository->create($data);

                    if(!$this->result['error'])
                    {
                        $created = true;
                    }
                    else
                    {
                        $created = false;
                    }

                }
                else
                {
                   

                    $created =false;

                    $this->result['data'] = false;
                    $this->result['error'] = true;
                    $this->result['error-messages']['validation-messages'] = $this->createStaffValidation->getErrors();
                    $this->result['validation-messages'] = true;
                }



            }


            
        } catch (Exception $e) 
        {
            /*
             * Log service errors
             */
            
            /*
             * Rollback transaction if a transaction was started
             */
            if($this->staffRepository->getTransactionLevel() > 0)
            {
                $this->staffRepository->rollbackTransaction();
            }   
            
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $this->result['successs-message'] = false;
            $this->result['error'] = true;
            $this->result['error-messages']['server-error'] = "Unable to save record"; 
        }
        
        
        return $created;
    }
    
    
    /*
     * Service method to confirm a staff's account
     * via email confirmation link
     * @params $confirmationCode
     */
    public function confirmStaffAccount($confirmationCode)
    {
        $confirmed = false;
        
        try
        {
            
            $filter =[
                    
                'confirmation_code' => $confirmationCode,
                'confirmed'         => 0
                    
            ];
            
            if($this->confirmStaffValidation->passes($filter))
            {
                
                if($this->userRepository->existsWhere($filter))
                {
                   
                    /*
                     * Set user data
                     */
                    $userData = [
                        'confirmed'             => 1,
                        'confirmation_code'     => ''
                    ];
                    
                    /*
                     * Update the user
                     */
                    $dbResult = $this->userRepository->updateWhere($userData, $filter);

                    if(!$dbResult['error'])
                    {
                        $confirmed = true;
                        $this->result['data'] = $dbResult['data'];
                        $this->result['error'] = true;
                        $this->result['success-message'] = "Successfully confirmed account";
                    }
                    else
                    {
                        $confirmed = false;
                        $this->result['data'] = false;
                
                        $this->result['error'] = true;
                        $this->result['error-messages']['server-error'] = "There was a problem confirming the user account";
                    }
                    
                }
                else
                {
                    $confirmed = false;
            
                    $this->result['error-messages']['not-found'] = "The specified resource does not exist";
                    $this->result['error'] = true;
                }
            
            }
            else
            {
                $confirmed =false;

                $this->result['data'] = false;
                
                $this->result['error'] = true;
                $this->result['error-messages']['validation-errors'] = $this->createStaffValidation->getErrors();
            }
            
        } catch (Exception $e) 
        {
            
            /*
             * Log service errors
             */
            
            /*
             * Rollback transaction if a transaction was started
             */
            if($this->staffRepository->getTransactionLevel() > 0)
            {
                $this->userRepository->rollbackTransaction();
            }
            
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $this->result['error'] = true;
            $this->result['error-messages']['server-error'] = "Unable to save record"; 
            $confirmed = false;
        }
        
        return $confirmed;
        
    }
    
   // ------- HELPER METHODS --------- //
    
    public function removeIsUserFromData(&$data)
    {
        unset($data['is_user']);
    }
    
    public function removeHasRoleFromArray(&$data)
    {
        unset($data['has_role']);
    }
    
    public function removeRoleArrayFromData(&$data){
        unset($data['role_id']);
    }
    
    public function setHasRoleFlag($hasRole = false)
    {
        $this->hasRole = $hasRole;
    }
    
    public function setUserRoles(array $userRoles = [])
    {
        $this->userRoles = $userRoles;
    }
    
    
    /*
     * Helper method to generate a random string for password when a user is signing in
     */
    function generateRandomString($length = 10) 
    {
        
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
    
        return $randomString;
    
    }

}
