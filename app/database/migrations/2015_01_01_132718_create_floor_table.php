<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFloorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('floor', function(Blueprint $table){
                
                $table->increments('id');
                $table->string('floor_name', 40)->default('');
                $table->string('floor_code', 40)->default('');
                
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->softDeletes();
                
                $table->char('created_by_user_id', 36)->default(0);
                $table->char('deleted_by_user_id', 36)->default(0);

            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('floor');
	}

}
