<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Creates the roles table
        Schema::create('role', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique();
            $table->string('description')->default('');
            $table->timestamps();
            $table->softDeletes();
        });

        // Creates the assigned_roles (Many-to-Many relation) table
        Schema::create('assigned_role', function ($table) {
            $table->increments('id')->unsigned();
            $table->char('user_id', 36);
            $table->integer('role_id')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();
//            $table->foreign('user_id')->references('id')->on('user')
//                ->onUpdate('cascade')->onDelete('cascade');
//            $table->foreign('role_id')->references('id')->on('role');
        });

        // Creates the permissions table
        Schema::create('permission', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name')->unique()->default('');
            $table->string('display_name')->default('');
            $table->timestamps();
            $table->softDeletes();
        });

        // Creates the permission_role (Many-to-Many relation) table
        Schema::create('permission_role', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('permission_id')->unsigned()->default(0);
            $table->integer('role_id')->unsigned()->default(0);
//            $table->foreign('permission_id')->references('id')->on('permission'); // assumes a users table
//            $table->foreign('role_id')->references('id')->on('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
//        Schema::table('assigned_role', function (Blueprint $table) {
//            $table->dropForeign('assigned_roles_user_id_foreign');
//            $table->dropForeign('assigned_roles_role_id_foreign');
//        });

//        Schema::table('permission_role', function (Blueprint $table) {
//            $table->dropForeign('permission_role_permission_id_foreign');
//            $table->dropFore1ign('permission_role_role_id_foreign');
//        });

        Schema::drop('assigned_role');
        Schema::drop('permission_role');
        Schema::drop('role');
        Schema::drop('permission');
    }

}
