<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetValuationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('asset_valuation', function(Blueprint $table){
                $table->char('id', 36)->primary();
                $table->decimal('book_value', 16,4)->default(0);
                $table->timestamp('valuated_at')->default('0000-00-00 00:00:00');
                        
                 $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->softDeletes();

                $table->char('created_by_user_id', 36)->default(0);
                $table->char('deleted_by_user_id', 36)->default(0);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('asset_valuation');
	}

}
