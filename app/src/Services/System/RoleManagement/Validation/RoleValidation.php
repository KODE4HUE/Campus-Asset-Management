<?php namespace App\Services\System\RoleManagement\Validation;

use App\Services\Validation\CustomValidation;

/**
 * Description of CreateRoleValidation
 *
 * @author hue
 */
class RoleValidation extends CustomValidation 
{

    //put your code here
    protected static $rules = [
        'name' => 'required'
    ];
            
}
