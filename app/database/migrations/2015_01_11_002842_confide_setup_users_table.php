<?php

use Illuminate\Database\Migrations\Migration;

class ConfideSetupUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        // Creates the users table
        Schema::create('user', function ($table) {
            $table->char('id', 36)->primary();
            $table->string('username')->unique();
            $table->char('staff_id', 36)->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('first_login')->default(false);
            $table->string('confirmation_code');
            $table->string('remember_token')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        // Creates password reminders table
        Schema::create('password_reminder', function ($table) 
        {
            $table->string('email');
            $table->string('token');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('password_reminder');
        Schema::drop('user');
    }
}
