<?php namespace App\Services\System\RoleManagement\Validation;

use App\Services\CustomValidation;

/**
 * Description of AssociatePermissionWithRole
 *
 * @author hue
 */
class PermissionWithRoleValidation extends CustomValidation 
{
    public static $rules = [
        'permission_id'     => 'required|numeric',
        'role_id'           => 'required|numeric',
    ];
}
