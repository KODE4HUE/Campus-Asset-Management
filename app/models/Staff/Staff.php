<?php namespace Model\Staff;

use Illuminate\Database\Eloquent\Model;

class Staff extends \Model\System\UuidModel
{
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;
                                        
    protected $table = 'staff';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    
    protected $fillable = [
        'staff_number',
        'first_name',
        'last_name',
        'gender_id',
        'cell_phone_number',
        'job_desscription',
        'address_line',
        'is_user',
        'profile_image',
        'email'
    ];
    
    


    public function user()
    {
        return $this->hasOne('Model\System\User');
    }
}
