<?php namespace Model\System;

use Illuminate\Database\Eloquent\Model;


class UserRole extends Model
{
    protected $table = 'assigned_role';
    
    protected $fillable = [
        'role_id',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
 
}
