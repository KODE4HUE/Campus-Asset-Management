<?php namespace App\Services\Asset\Validation;

use App\Services\CustomValidation;

class CreateAssetModelValidation extends CustomValidation
{
    protected  static $rules = [
      'asset_name'                  => 'required',
      'product_code'                => 'required',
      'depreciation_method_id'      => 'required',
      'asset_type_id'               => '',
      'manufacturer'                => 'required',
      'description'                 => '',
      
    ];
    
    protected $messages = [
        'asset_type_id.required'             => 'The Asset Type is required',
        'product_code.required'              => 'The Product Code is required',
        'depreciation_method_id.required'    => 'The Depreciation Method is required',
        'manufacturer.required'              => 'The Manufacturer is required',
        
    ];
    
    
    
}
