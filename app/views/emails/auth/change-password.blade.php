Hello, {{$full_name}}

The password for your <a href='#'>UCAMS</a> account with Staff ID {{$staff_number}} was changed.

<br /> <br />

If you authorized the change, you can ignore this email.

<br /> <br />

If it was not you, your account has been compromised. Please carry the following step(s):

<br /> <br />

1. <a href="#">Reset your password</a>

<br /> <br />

Thanks,
The UCAMS team.




