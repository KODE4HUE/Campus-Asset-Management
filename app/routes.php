<?php

//use Cartalyst\Sentry\Sentry;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Model\System\User;


//use Symfony\Component\Routing\Route;
//use Symfony\Component\Routing\Route;

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/*
  | Authenticated routes
 */ 

Route::group(['prefix' => 'api'], function()
{

    Route::group(array('before' => 'jwt-auth'), function()
    {


        // *************************************
        // ******** User Routes ****************
        //**************************************

        Route::post('/user/all', [
            'as'    =>  'user/all',
            'uses'  =>  'UserController@getAllUsers'
        ]);

        Route::post('/user/{id}', [
            'as'    =>  'user/get',
            'uses'  =>  'UserController@getUser'
        ]);

        Route::post('/user/create', [
            'as'    =>  'user/create',
            'uses'  =>  'UserController@createUser'
        ]);

        Route::put('/user/{id}', [
            'as'    =>  'user/update',
            'uses'  =>  'UserController@updateUser'
        ]);

        Route::delete('/user/{id}', [
            'as'    =>  'user/archive',
            'uses'  =>  'UserController@archiveUser'
        ]);

        Route::get('/user/staff/current/',[
            'as'    =>  'user/staff/current',
            'uses'  =>  'StaffController@getLoggedInStaff'
        ]);

         // *************************************
        // ******** Staff Routes ****************
        //**************************************

        Route::get('/staff/all', [
            'as'    =>  'staff/all',
            'uses'  =>  'StaffController@getAllStaff'
        ]);

        Route::get('/staff/{id}', [
            'as'    =>  'staff/get',
            'uses'  =>  'StaffController@getStaff'
        ]);


        Route::post('/staff/create', [
            'as'    =>  'staff/create',
            'uses'  =>  'StaffController@createStaff'
        ]);

        Route::put('/staff/{id}', [
            'as'    =>  'staff/update',
            'uses'  =>  'StaffController@updateStaff'
        ]);

        Route::delete('/staff/{id}', [
            'as'    =>  'staff/archive',
            'uses'  =>  'StaffController@archiveStaff'
        ]);

        // *************************************
        // ******** Role Routes ****************
        //**************************************

        Route::post('/role/all', [
            'as'    =>  'role/all',
            'uses'  =>  'RoleController@getAllRoles'
        ]);

        Route::post('/role/{id}', [
            'as'    =>  'role/get',
            'uses'  =>  'RoleController@getRole'
        ]);

        Route::post('/role/create', [
            'as'    =>  'role/create',
            'uses'  =>  'RoleController@createRole'
        ]);

        Route::put('/role/{id}', [
            'as'    =>  'role/update',
            'uses'  =>  'RoleController@updateRole'
        ]);

        Route::put('/role/{id}', [
            'as'    =>  'role/archive',
            'uses'  =>  'RoleController@archiveRole'
        ]);


        Route::post('/user/role/add', [
            'as'    =>  '/user/role/add',
            'uses'  =>  'RoleController@addUserToRole'
        ]);

        Route::delete('/user/{userId}/role/{roleId}', [
            'as'    =>  '/user/remove',
            'uses'  =>  'RoleController@removeRoleFromUser'
        ]);

        Route::post('role/{id}/permission/add', [
            'as'    =>  '/role/permission/add',
            'uses'  =>  'PermissionController@associatePermissionWithRole'
        ]);

        Route::delete('role/{roleId}/permission/{permissionId}/add', [
            'as'    =>  '/role/permission/delete',
            'uses'  =>  'PermissionController@addPermissionToRole'
        ]);

        // *************************************
        // ******** Permission Routes **********
        //**************************************

        Route::get('/permission/browse', [
            'as'    =>  'permission/all',
            'uses'  =>  'PermissionController@getAllPermissions'
        ]);

        Route::get('/permission/{id}', [
            'as'    =>  'permission/get',
            'uses'  =>  'PermissionController@getPermission'
        ]);

        Route::post('/permission/create', [
            'as'    =>  'permission/create',
            'uses'  =>  'PermissionController@createPermission'
        ]);

        Route::put('/permission/{id}', [
            'as'    =>  'permission/update',
            'uses'  =>  'PermissionController@updatePermission'
        ]);

        Route::delete('/permission/{id}', [
            'as'    =>  'permission/archive',
            'uses'  =>  'PermissionController@archivePermission'
        ]);

        
         // ****************************************************
        // ******** Asset Model Routes (Protected) **********
        //****************************************************
        
        Route::get('/asset-model/browse', [
            'as'    =>  'asset-model/browse',
            'uses'  =>  'AssetModelController@getAssetModels'
        ]);
        
        Route::get('/asset-model/{id}', [
            'as'    =>  'asset-model/get',
            'uses'  =>  'AssetModelController@getAssetModel'
        ]);
        
        Route::post('/asset-model/create', [
            'as'    =>  'asset-model/create',
            'uses'  =>  'AssetModelController@createAssetModel'
        ]);
        
        Route::put('/asset-model/{id}', [
            'as'    =>  'asset-model/update',
            'uses'  =>  'AssetModelController@updateAssetModel'
        ]);
        
        Route::delete('/asset-model/{id}', [
            'as'    =>  'asset-model/archive',
            'uses'  =>  'AssetModelController@archiveAssetModel'
        ]);
        
        Route::get('/dropdown/asset-models', [
           'as'     =>  'dropdown/asset-models',
            'uses'  =>  'AssetModelController@getSelectable'
        ]);
        
        // ****************************************************
        // ******** Asset  Routes (Protected) **********
        //****************************************************
        
        Route::get('/asset/browse', [
            'as'    =>  'asset/browse',
            'uses'  =>  'AssetController@getAssets'
        ]);
        
        Route::get('/asset/{id}', [
            'as'    =>  'asset/get',
            'uses'  =>  'AssetController@getAsset'
        ]);
        
        Route::post('/asset/create', [
            'as'    =>  'asset/create',
            'uses'  =>  'AssetController@createAsset'
        ]);
        
        Route::put('/asset/{id}', [
            'as'    =>  'asset/update',
            'uses'  =>  'AssetController@updateAsset'
        ]);
        
        Route::delete('/asset/{id}', [
            'as'    =>  'asset/archive',
            'uses'  =>  'AssetController@archiveAsset'
        ]);
        
        Route::get('/dropdown/assets', [
           'as'     =>  'dropdown/assets',
            'uses'  =>  'AssetController@getSelectable'
        ]);
        
        // ****************************************************
        // ***** Asset Transaction Routes (Protected) ********
        //****************************************************
        
        // ****************************************************
        // ******** Leased Asset Routes (Protected) **********
        //****************************************************
        
         // ****************************************************
        // ******** Authentication Routes (Protected) **********
        //****************************************************
        Route::post('/auth/change-password', [
            'as'   => 'auth/change-password',
            'uses' => 'AuthenticationController@changeLoggedInUserPassword'
        ]);
        
        Route::get('/auth/logged-in-user', [
           'as'     => 'auth/logged-in-user',
            'uses'  => 'AuthenticationController@getLoggedInUser'
        ]);
    });

    Route::post('/auth/token', [
        'as'   => 'auth/token',
        'uses' => 'AuthenticationController@store'
    ]);

    Route::get('/staff/account-activate/{confirmationCode}', [
          'as'        =>  'staff/account-confirm',
          'uses'      => 'StaffController@confirmStaffAccount'
    ]);
    
});

// Confide routes
//Route::get('users/create', 'UsersController@create');
//Route::post('users', 'UsersController@store');
//Route::get('users/login', 'UsersController@login');
//Route::post('users/login', 'UsersController@doLogin');
//Route::get('users/confirm/{code}', 'UsersController@confirm');
//Route::get('users/forgot_password', 'UsersController@forgotPassword');
//Route::post('users/forgot_password', 'UsersController@doForgotPassword');
//Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
//Route::post('users/reset_password', 'UsersController@doResetPassword');
//Route::get('users/logout', 'UsersController@logout');
