<?php namespace App\Repositories\System\UserManagement\UserRole;

use App\Repositories\BaseRepository;
use App\Repositories\System\UserManagement\UserRole\IUserRoleRepository;
use Model\System\UserRole;

class UserRoleRepository extends BaseRepository implements IUserRoleRepository
{
    protected $userRole;
    
    public function __construct(UserRole $userRole, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage=25)
    {
        parent::__construct($userRole, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        
        $this->userRole = $userRole;
    }
    
}
