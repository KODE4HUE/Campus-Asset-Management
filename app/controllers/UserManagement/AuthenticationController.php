<?php

use App\Services\System\UserManagement\AuthenticationServiceManager;
use Illuminate\Support\Facades\Response;

class AuthenticationController extends BaseController
{
    
     protected static $restful = true;
     protected $authenticationServiceManager;


    public function __construct(AuthenticationServiceManager $authenticationServiceManager)
    {
        $this->authenticationServiceManager = $authenticationServiceManager;
    }
     
     
    public function getLoggedInUser()
    {
         $statusCode = 200;
         
         $this->authenticationServiceManager->getCurrentlyLoggedInUser();
         
         $result = $this->authenticationServiceManager->getResult();
        
        if(!$result['error'])
        {
           $statusCode = 200;
        }
        else
        {
            
            if($result['error-messagess']['not-found'])
            {
                $statusCode = 404;
            }
            
            if($result['error-messages']['server-error'])
            {
                $statusCode = 500;
            }
            
        }
        
        return Response::json($result, $statusCode);
    }

    /**
     * Returns a newly created access token for authorized users
     *
     * @return Response
     */
    public function store()
    {  
        
         $credentials = Input::only('username', 'password');
         
         if($this->authenticationServiceManager->generateAuthenticationToken($credentials))
         {
             $statusCode = 200;
         }
         else
         {
             $statusCode = 401;
         }
        
         return Response::json($this->authenticationServiceManager->getResult(), $statusCode);
    }
    
    /**
     * Changess a user's password for the currently logged in user
     */
    public function changeLoggedInUserPassword()
    {
        $credentials =  Input::only('old_password', 'password', 'confirm_password');
        
        $this->authenticationServiceManager->changeUserPassword($credentials);
        
        $result = $this->authenticationServiceManager->getResult();
        
        if(!$result['error'])
        {
             $statusCode = 200;
        }
        else
        {
            
           if($result['error-messages']['validation-messages'])
           {
               $statusCode = 400;
           }
           else
           {
               $statusCode = 500;
           }

        }
        
        return Response::json($result, $statusCode);
    }
   

}
