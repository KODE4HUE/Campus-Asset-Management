<?php namespace Model\Asset;

use Model\System\UuidModel;

class AssetModel extends UuidModel
{
    
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;
    
    protected $table = 'asset_model';
    
    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    
    protected $fillable = [
        'asset_name',
        'product_code',
        'asset_model_image',
        'depreciation_method_id',
        'manufacturer',
        'description',
        'created_by_user_id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    
}
