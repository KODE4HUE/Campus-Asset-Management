<?php namespace App\Repositories\Asset\LeasedAsset;

use App\Repositories\Asset\LeasedAsset\ILeasedAssetRepository;
use App\Repositories\BaseRepository;
use Model\Asset\LeasedAsset;

/**
 * Description of LeasedAssetRepository
 *
 * @author hue
 */
class LeasedAssetRepository extends BaseRepository implements ILeasedAssetRepository {
    
    public function __construct(LeasedAsset $leasedAsset, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
