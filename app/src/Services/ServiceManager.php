<?php namespace App\Services;

/**
 * Description of ServiceManager
 *
 * @author hue
 */
abstract class ServiceManager 
{
    
    protected $result = [
        "success-message"           =>  "",
        "error"                     =>  false,
        "error-messages"            =>  [
            "validation-messages"   =>  false,
            "server-error"          =>  false,
            "not-found"             =>  false
        ],
        "data"                      =>  false
    ];
    
    public function getResult()
    {
        return $this->result;
    }
    
    public function resetResult()
    {
        $this->result = [
            "error"                     =>  false,
            "error-messages"            =>  [
                "validation-messages"   =>  false,
                "server-error"          =>  false,
                "not-found"             =>  false
            ],
            "data"                      =>  false
        ];
    }
            
}
