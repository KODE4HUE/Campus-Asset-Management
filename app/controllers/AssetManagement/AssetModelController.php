<?php

use App\Services\Asset\AssetModelServiceManager;

class AssetModelController extends BaseController 
{
    protected $restful = true;
    protected $assetModelServiceManager;

    public function __construct(AssetModelServiceManager $assetModelServiceManager) 
    {
        $this->assetModelServiceManager = $assetModelServiceManager;
    }
    
    /**
     * Route for getting the asset models/prototypes
     */
    public function getAssetModels()
    {
        $statusCode = 200;
        
        $this->assetModelServiceManager->getAssetModels();
        
        $result = $this->assetModelServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
        
    }
    
    public function getAssetModel($id)
    {
        $statusCode = 200;
        
        
        $this->assetModelServiceManager->getAssetModel($id);
        
        $result = $this->assetModelServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }
    
    public function createAssetModel()
    {
        $statusCode = 200;
        
        $input = Input::only('asset_name', 'product_code', 'asset_model_image', 'asset_type_id', 'depreciation_method_id', 'manufacturer', 'description');
        
        $this->assetModelServiceManager->createAssetModel($input);
        
        $result = $this->assetModelServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 400;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }
    
    public function updateAssetModel($id)
    {
        $input = Input::only('asset_name', 'product_code', 'asset_model_image', 'asset_type_id', 'depreciation_method_id', 'manufacturer', 'description');
        $input['id'] = $id;
        
        $this->assetModelServiceManager->updateAssetModel($input);
        
        $result = $this->assetModelServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }
    
    public function archiveAssetModel($id)
    {
        $statusCode = 200;
        
        $input = [
            'asset_model_id' => $id
        ];
        
        $this->assetModelServiceManager->archiveAssetModel($input);
        
        $result = $this->assetModelServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }
    
    public function getSelectable()
    {
        $statusCode = 200;
        
        $this->assetModelServiceManager->getSelectable();
        
        $result = $this->assetModelServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }   
}
