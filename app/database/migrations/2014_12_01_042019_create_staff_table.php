<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('staff', function(Blueprint $table)
        {

            $table->char('id', 36)->primary();           
            $table->string('email', 255)->default('');
            $table->string('staff_number', 128)->default('');
            $table->string('first_name',128)->default('');
            $table->string('last_name', 128)->default('');
            $table->integer('gender_id')->unsigned()->default(0);
            $table->string('job_description', 150)->default('');
            $table->binary('profile_image');
            $table->string( 'cell_phone_number' ,16)->default('');
            $table->string('address_line', 300)->default('');
             
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();

            $table->char('created_by_user_id', 36)->default('');
            $table->char('deleted_by_user_id', 36)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('staff');
    }

}
