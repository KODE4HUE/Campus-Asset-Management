<?php namespace App\Services\Asset;

use App\Repositories\Asset\AssetModel\IAssetModelRepository;
use App\Repositories\System\Authentication\IAuthenticationRepository;
use App\Services\Asset\Validation\CreateAssetModelValidation;
use App\Services\ServiceManager;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

class AssetModelServiceManager extends ServiceManager
{
    
    protected $authenticationRepository;
    protected $assetModelRepository;
    protected $createAssetModelValidation;
    


    public function __construct( 
        IAssetModelRepository $assetModelRepository,
        IAuthenticationRepository $authenticationRepository,
        CreateAssetModelValidation $createAssetModelValidation
    )
    {
        $this->assetModelRepository = $assetModelRepository;
        $this->authenticationRepository = $authenticationRepository;
        $this->createAssetModelValidation = $createAssetModelValidation;
    } 
    
    public function getAssetModels()
    {
        $hasData = false;
        $this->resetResult();
        
        $errorMessages = new MessageBag();
        
        $dbResult = $this->assetModelRepository->all();
        
         if($dbResult['error'])
        {
            $this->result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
            
            if(!$dbResult['data'])
            {
                $this->result["error"]  = true;
                $this->result['error-messages']['not-found'] = "No results found";
                $hasData = false;    
            }
            else
            {
                $this->result["success-message"] = "successfully retrieved records";
                $this->result['data'] = $dbResult['data'];
                $hasData = true;
            }
        }
        
        return $hasData;
        
    }
    
    public function getAssetModel($id)
    {
        $hasData = false;
        
        $dbResult = $this->assetModelRepository->get($id);
        
        if($dbResult['error'])
        {
            $this->result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
             
            if(!$dbResult['data'])
            {
                $this->result["error"]  = true;
                $this->result['error-messages']['not-found'] =  "No results found";
                $hasData = false;  
            }
            else
            {
                $this->result['data'] = $dbResult['data'];
                $this->result["success-message"] = "successfully retrieved record";
                $hasData = true;
            }
        }
        
        
        return $hasData;
    }


    public function createAssetModel($data)
    {
        $created = false;
        
        try 
        {   
            // get currently logged in user information
            $currentlyLoggedInUserResult = $this->authenticationRepository->getLoggedInUser();
            
            if($currentlyLoggedInUserResult['error'])
            {
                throw new Exception('Could not retrieve currently logged in user');
            }
            
            /**
             * Sets the asset model image to empty string if a file has not been provided
             */
            if(isset($data['asset_model_image']))
            {
                if(empty($data['asset_model_image']))
                {
                   $data['asset_model_image'] = ""; 
                }
                else
                {
                    $data['asset_model_image'] = "";
                }
            }
            else
            {
                $data['asset_model_image'] = "";
            }
            
            if(isset($data['description']))
            {
                if(empty($data['description']))
                {
                   $data['description'] = ""; 
                }
                else
                {
                    $data['decription'] = "";
                }
            }
            else
            {
                $data['description'] = "";
            }
            
            if($this->createAssetModelValidation->passes($data))
            {
                
               // dd('herdde');
                // asset model data valid - so save asset model to database
                $dbResult = $this->assetModelRepository->create($data);
                        
                
                if($dbResult['error'])
                {
                    throw new Exception("There was an error saving the new asset model to the database");
                }
                else
                {
                    $this->result["success-message"] = "successfully created record";
                    $this->result['data'] = $dbResult['data'];
                    $created = true;
                }
                
            }
            else
            {
                // asset model data invalid - so return validation errors
                $created =false;
                $this->result['data'] = false;
                $this->result['error'] = true;
                $this->result['error-messages']['validation-messages'] = $this->createAssetModelValidation->getErrors();
            }
            
        } catch (Exception $e) 
        {
            /*
             * Log service errors
             */
            
            /*
             * Rollback transaction if a transaction was started
             */
            if($this->assetModelRepository->getTransactionLevel() > 0)
            {
                $this->assetModelRepository->rollbackTransaction();
            }   
            
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $this->result['successs-message'] = false;
            $this->result['error'] = true;
            $this->result['error-messages']['server-error'] = "Unable to save asset model"; 
            $created = false;

        }
        
        return $created;
    }
    
    public function updateAssetModel($data)
    {

         $updated = false;
         $this->resetResult();
         
         if($this->createAssetModelValidation->passes($data))
         {
             
            if($this->assetModelRepository->exists($data['id']))
            {
                
                //$data['name'] = strtolower( str_replace(' ', '_', $data['display_name']));

                $dbResult = $this->assetModelRepository->update($data);

                if(!$dbResult['error'])
                {
                    $this->result['success-message'] = "Successfully updated resource";
                }
                else
                {
                    $this->result['error-messages']['server-error'] = "There was a problem while updating the specified resource";
                }
                
            }
            else
            {
                $updated = false;
            
                $this->result['error-messages']['not-found'] = "The specified resource does not exist";
                $this->result['error'] = true;
                
            }
             
         }
         else
         {
            $updated =false;
            
            $this->result['error-messages']['validation-messages'] = $this->permissionValidation->getErrors();
            $this->result['error'] = true;
           
         }
          
         return $updated;
    }
    
    public function archiveAssetModel($id)
    {
        $archived = false;
        $this->resetResult();
        
        //check if item does not exist or is already archived
        if($this->assetModelRepository->exists($id))
        {
            
            $dbResult = $this->assetModelRepository->archive($id);
            
            if(!$dbResult['error'])
            {
                $archived = true;
                $this->result['success-message'] = "Successfully archived permission";
                $this->result['error'] = true;
            }
            else
            {
                $archived = false;
                $this->result['error-messages']['server-error'] = "Unable to archive permission";
            }
             
        }
        else
        {
            // the specified resourse could not be found
            $archived =false;
            
            $this->result['error'] = true;
            $this->result['error-messages']['not-found'] = "The specified resource could already be archived or does not exist";
        }
        
        return $archived;
    }
    
    public function getSelectable($displayName = 'asset_name')
    {
        $hasData = false;
        
        $dbResult = $this->assetModelRepository->getSelectable($displayName);
        
        if($dbResult['error'])
        {
            $this->result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resources";
            $hasData = false;
        }
        else
        {
             
            if(!$dbResult['data'])
            {
                $this->result["error"]  = true;
                $this->result['error-messages']['not-found'] =  "No results found";
                $hasData = false;  
            }
            else
            {
                $this->result['data'] = $dbResult['data'];
                $this->result["success-message"] = "successfully retrieved list";
                $hasData = true;
            }
        }
        
        return $hasData;
    }
}
