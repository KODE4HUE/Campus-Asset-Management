<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetDepreciationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('asset_depreciation', function(Blueprint $table){
                $table->char('id', 36)->primary();
                $table->char('asset_id', 36)->default('');
                $table->decimal('asset_cost', 18,4)->default(0);
                $table->decimal('depreciating_value',18, 4)->default(0);
                $table->decimal('residual_value', 18,4)->default(0);
                $table->integer('depreciation_type_id')->unsigned()->default(0);
                $table->integer('depreciation_method_id')->unsigned()->default(0);
                $table->date('period_start');
                $table->date('period_end');
                $table->integer('number_of_years')->unsigned()->default(0);        
                
            });
            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('asset_depreciation');
	}

}
