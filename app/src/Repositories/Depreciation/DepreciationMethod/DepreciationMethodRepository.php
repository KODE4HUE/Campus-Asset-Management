<?php namespace App\Repositories\Depreciation\DepreciationMethod;

use App\Repositories\BaseRepository;
use App\Repositories\Depreciation\DepreciationMethod\IDepreciationMethodRepository;

/**
 * Description of DepreciationMethodRepository
 *
 * @author hue
 */
class DepreciationMethodRepository extends BaseRepository implements IDepreciationMethodRepository {

    //put your code here
    protected $depreciationMethod;
    
    public function __construct(\Model\DepreciationMethod\DepreciationMethod $depreciationMethod, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->depreciationMethod = $depreciationMethod;
    }
}
