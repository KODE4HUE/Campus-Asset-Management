<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('school', function(Blueprint $table){
                $table->increments('id');
                $table->string('school_name', 128)->default('');
                $table->string('school_code', 128)->default('');
                $table->integer('parent_faculty_id')->unsigned()->default(0);
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->char('created_by_user_id', 36)->default(0);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
                Schema::drop('school');
	}

}
