<?php namespace Model\System\RoleManagement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of PermissionRole
 *
 * @author hue
 */
class PermissionRole extends \Illuminate\Database\Eloquent\Model
{

     use \Illuminate\Database\Eloquent\SoftDeletingTrait;
     
    protected $table = 'permission_role';
    
}
