<?php namespace App\Services\Staff\Validation;

use App\Services\CustomValidation;


/*
 * Validation class for when confirming a new staff via link
 */
class ConfirmStaffValidation  extends CustomValidation
{
    
    protected  static $rules = [
      'confirmation_code' => 'required',
      'confirmed'         => 'required'
    ];
    
    protected static $messages = [
        
    ];
    
}
