<?php

use App\Services\RoleManagement\RoleServiceManager;

/**
 * Description of RoleController
 *
 * @author hue
 */
class RoleController extends BaseController
{
    
    protected static $restful = true;
    protected $roleServiceManager;

    public function __construct(RoleServiceManager $roleServiceManager)
    {
        $this->roleServiceManager = $roleServiceManager;
    }
   
    public function index()
    {
        $statusCode = 200;
        
        if($this->roleServiceManager->getAllRoles())
        {
            $statusCode = 200;
        }
        else
        {
            $statusCode = 401;
        }
        
        
         return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
    
    public function store()
    {
        $statusCode = 401; 
        $input = Input::only('role_name', 'description');
        
        if($this->roleServiceManager->createRole($input))
        {
            $statusCode = 200;
        }
        else
        {
            $statusCode =401; // bad request made
        } 
        
        return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
    
    public function update($roleId)
    {
        $statusCode = 200; 
        $input = Input::only('role_name', 'description');
        $input['id'] = $roleId;
        
        if($this->roleServiceManager->updateRole($input))
        {
            $statusCode = 200;
        }
        else
        {
            $statusCode = 401;
        }
        
        return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
    
    public function delete()
    {
        $statusCode = 200;
        $input = Input::only('id');
        
        if($this->roleServiceManager->archiveRoleById($input['id']))
        {
            $statusCode = 200;
        }
        else
        {
            $statusCode = 401;
        }
        
        return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
    
    public function addUserToRole()
    {
        $statusCode = 200;
        
        $input = Input::only('user_id', 'role_id');
        
        if($this->roleServiceManager->addUserToRole($input['user_id'], $input['role_id']))
        {
            $statusCode = 200;
        }
        else
        {
            $statusCode = 401; //bad request
        }
        
        return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
    
    public function removeUserFromRole()
    {
         $statusCode = 200;
        
        $input = Input::only('user_id', 'role_id');
        
        if($this->roleServiceManager->removeUserToRole($input['user_id'], $input['role_id']))
        {
            $statusCode = 200;
        }
        else
        {
            $statusCode = 401; //bad request
        }
        
        return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
}
