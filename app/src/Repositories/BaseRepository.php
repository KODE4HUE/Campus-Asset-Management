<?php namespace App\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;

abstract class BaseRepository
{

    protected $model;
    protected $sortByColumn;
    protected $sortByOrder;
    protected $itemPerPage;

    public function __construct($model, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        $this->model = $model;
        $this->sortByColumn = $_sortByColumn;
        $this->sortByOrder = $_sortByOrder;
        $this->itemPerPage = $_itemPerPage;
    }
    
    
    public function all(array $related = null)
    {
        $result['data'] = false;
        $result['error'] = false;
        
        try
        {
            $result['data'] = $this->model->all();
            
            if (isset($related))
            {
                $result['data'] = $this->model->find($id)->has($related)->all();
            }
            else
            {
                
                $data = $this->model->all();
                
                //dd($data->isEmpty());
                
                if(!$data->isEmpty()){
                  $result['data'] = $data->toArray();
                }
                else
                {
                    $result['data'] = false;
                }
                
                
            }
            
            
        } 
        catch (Exception $e) 
        {
            
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag(['server-error'  => "There was an error trying to fetch the specified resources"]);
        }
        
        return $result;
        
    }

    public function find($filter = array())
    {
        $result['data'] = false;
        $result['error'] = false;

        try
        {
            if (is_array($filter))
            {
                foreach ($filter as $key => $value)
                {
                    if ($value['condition'])
                    {
                        $this->model = $this->model->where($value['column'], $value['condition'], $value['value']);
                    }
                    else
                    {
                        $this->model = $this->model->where($key, "=", $value);
                    }
                }
            }

            if ($this->sortByColumn)
            {
                $this->model = $this->model->orderBy($sortBy, $order);
            }

            if ($this->itemPerPage)
            {
                $this->model = $this->model->paginate($this->itemPerPage);
            }

            $result['data'] = $this->model->get();
            
        } catch (Exception $e)
        {

            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages']['server-error'] = 'There was an error retrieving the specified resource';
        }

        return $result;
    }

    public function get($id, $related = null)
    {
        
      
        $result['data'] = false;
        $result['error'] = false;
       

        try
        {
            if (isset($related))
            {
                $result['data'] = $this->model->find($id)->has($related)->get();
            }
            else
            {
                  
                $data = $this->model->find($id);      
                
                if($data !== null){
                     $result['data'] = $data->toArray();
                }
                else{
                    $result['data'] = false;
                }
                        
            }
            
            $result['error'] = false;
            
        } catch (Exception $e)
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag(['server-error'  => "There was an error trying to fetch the specified resource"]);

        }

        return $result;
    }

    public function getWhere($column, $value, array $related = null)
    {

        $result['data'] = false;
        $result['error'] = false;

        try
        {
            $result['data'] = $this->model->where($column, '=', $value)->get();
        } catch (Exception $e)
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $result['error'] = true;
            $result['error-messages'] = new MessageBag(['server-error' => 'There was an error trying to fetch the specified resource']);

        }

        return $result;
    }

    public function getRecent($limit, array $related = null)
    {
        $result['data'] = false;
        $result['errors'] = false;

        try
        {
            $result['data'] = $this->model->where($column, '=', $value)->take($limit);
            
        } catch (Exception $e)
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new Messagebag(['server-error'  => 'There was an error trying to fetch the specified resource']);
        }

        return $result;
    }

    public function create(array $data)
    {
        
        $result['data'] = false;
        $result['server-error'] = false;
        
        try
        {
            
            $this->model->fill($data);
             
            $this->model->save();
            
            $result['error'] = false;
            $result['data'] = [
              
              'id'      => $this->model->id
            ];
           
        } catch (Exception $e)
        {
            
            /*
             *  log error message for developers to view in log file
             */
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . ' method');
            Log::debug($e->getTraceAsString());
            
            $result['error'] = true;
            $result['data']  = false;
            $result['error-messages'] =  new MessageBag(['server-error'  => 'There was an error trying to create the specified resource']);
        }
        
        return $result;
    }

    public function update(array $data)
    {

        $result['data'] = false;
        $result['error'] = false;

        try
        {
            
            $this->model = $this->model->find($data['id']);
            
              // unset id from data to be updated in the record since id is already fetched in eloquent model
            unset($data['id']);
            
            
            if($this->model === null)
            {
                
                throw new Exception("The record in table: ". $this->model->getTable()." with an id: ". $data["id"] ." was not found for updating");
            }
            
          

            foreach ($data as $key => $value)
            {
                $this->model->$key = $value;
                
            }
            
//            dd($this->model);
            
            if(!$this->model->save())
            {
                throw new Exception('Unable to update resource from via repo update');
            }
            
            $result['data'] = false;
            $result['error'] = false;
            $result['success-message'] =  "successfully updated resource";
            
        } catch (Exception $e)
        {

           dd($e->getMessage());
            

            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag([ 'server-error' => "There was an error trying to update the specified resource"]);
        }
        
        return $result;
    }
    
      /**
     * Update where  some filter matches the conditions specified
     * @param  $data
     * @return bool
     */
    public function updateWhere($data, $filter)
    {
        
        $result['data'] = false;
        $result['error'] = false;   
        
        try
        {
            
            if(is_array($filter))
            {
                
                foreach ($filter as $key => $value)
                {
                    
                   if(isset($value['condition'])) 
                   {
                       if(empty($value['condition']))
                       {
                            throw new Exception("Condition passed for where clause is empty");
                       }

                       $this->model = $this->model->where($value['column'], $value['condition'], $value['value']);
                   }
                   else
                   {
                       $this->model = $this->model->where($key, "=", $value);
                   }

                }
                
            }
             
            $this->model = $this->model->first();
            
            if($this->model === null)
            {
                
                throw new Exception("The record was not found for updating");
            }
            
            foreach ($data as $key => $value)
            {
                $this->model->$key = $value;
            }
            
            if(!$this->model->save())
            {
                throw new Exception('Unable to update resource from via repo update');
            }
            
           $result['data'] = false;
           $result['error'] = false;
            
        } catch (Exception $e) 
        {
            
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages']['server-error'] = "There was an error trying to update the specified resource";
        }
        
        return $result;
    }

    public function delete($id)
    {
        $result['data'] = false;
        $result['error'] = false;
        
        try
        {
            $result['data'] = $this->model->delete($id);
            
        } catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] =true;
            $result['error-messages'] = new MessageBag(['server-error' => "There was an error trying to archive the specified resource"]);
        }
        return $result;
   
    }

    public function deleteWhere($column, $value)
    {
        $result['data'] = false;
        $result['error'] = false;
        try
        {
            $result['data'] = $this->model->where($column, '=', $value)->delete();
            
        } catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag(['server-error' => "There was an error trying to archive the specified resource"]);
        }
        
        return $result;
    }
    
    public function archive($id)
        {
        $result['data'] = false;
        $result['error'] = false;
        
        try
        {
            
            
            //$this->model = $this->model->find($id);
            $this->model = $this->model->where('id', '=', $id)->first();
            
//            dd($this->model);
            
            if($this->model !== null)
            {
                
               if( $this->model->delete())
               {
                   $result['data']['archived'] = true;
               }
               else
               {
                   $result['data']['archived'] = false;
               }
               
            }
            else
            {
                $result['data']['archived'] = false;
            }
            
        } catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] =true;
            $result['error-messages']['server-error'] = "There was an error trying to archive the specified resource";
        }
        
        return $result;
    }
    
    public function archiveWhere($data)
    {
        
        $result['data'] = false;
        $result['error'] = false;
        
        $filter = $data['filter'];
        
        try
        {
            if (is_array($filter))
            {
                foreach ($filter as $key => $value)
                {
                    if ($value['condition'])
                    {
                        $this->model = $this->model->where($value['column'], $value['condition'], $value['value']);
                    }
                    else
                    {
                        $this->model = $this->model->where($key, "=", $value);
                    }
                }
            }
            
            $collection = $this->model->get();
            
            if(!$collection->isEmpty())
            {
                foreach($collection as $row)
                {
                    $row->deletedAt = date("Y-m-d H:i:s");
                    $row->deletedByUserId = $data('deleted_by_user_id');
                    $row->save();
                }  
                
                $result['error'] = false;
                $result['data']['archived'] = true;
            }
            else
            {
                $result['error'] = false;
                $result['data']['archived'] = false;
            }
            
        } catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $result['error'] = true;
            $result['error-messages'] = new MessageBag(["server-error" => "There was an error trying to archive the specified resource"]);
        }  
        
        return $result;
    }
    
    public function exists($id)
    {
        
        $exists = true;
        
        if( $this->model->where('id', '=', $id)->exists()){
            $exists = true;
        }
        else{
             $exists = false;
        }
        
        return $exists;
        
    }
    
    public function existsWhere(array $filter)
    {
        
        $exists = true;
        
        try
        {
            if (is_array($filter))
            {
                foreach ($filter as $key => $value)
                {
                    
                    if(isset($value['condition']))
                    {
                        if (empty($value['condition']))
                        {
                           throw new Exception("Condition passed for where clause is empty");
                        }
                        
                         $this->model = $this->model->where($value['column'], $value['condition'], $value['value']);
                    }
                    else
                    {
                        $this->model = $this->model->where($key, "=", $value);
                    }
                  
                  
                }
            }
            
            if($this->model->get() !== null)
            {
                $exists = true;
            }
            else
            {
                $exists = false;
            }
            
            
        } catch (Exception $e) 
        {
            
            dd($e->getMessage());
            
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            throw new Exception("Sorry an error occurred while checking if record exists in table named: " + $this->model->getTable());
        }
        
        return $exists;
    }
    
    public function getSelectable($displayName)
    {
        $result['data'] = false;
        $result['error'] = false;
        
        try
        {
            $result['data']=  $this->model->lists($displayName, 'id');
            
        } catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error=messages'] =  new MessageBag([ 'server-error' => "There was an error trying to retrieve the specified item(s)"]);
        }
        
        return $result;
        
    }

    public function orderByAndPaginate($sortBy, $order, $perPage)
    {
        
        $result['data'] = false;
        $result['error'] = false;
        
        try
        {
           $result['data'] = $this->model->orderBy($sortBy, $order)->paginate($perPage);
           
        } catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag(['server-error' => "There was an error trying to retrieve the specified item(s)"]);
        }

        return $result;
    }
    
    public function beginTransaction()
    {
        DB::beginTransaction();
    }
    
    public function commitTransaction()
    {
        DB::commit();
    }
    
    public function rollbackTransaction()
    {
        DB::rollback();
    }
    
    public function getTransactionLevel()
    {
        return DB::transactionLevel();
    }
    
    public function getModel()
    {
        return $this->model();
    }

}
