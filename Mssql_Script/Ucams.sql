USE [master]
GO
/****** Object:  Database [Utech_Assets]    Script Date: 01/12/2014 07:55:31 ******/
CREATE DATABASE [Utech_Assets]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Utech_Assets', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Utech_Assets.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Utech_Assets_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Utech_Assets_log.ldf' , SIZE = 1344KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Utech_Assets] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Utech_Assets].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Utech_Assets] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Utech_Assets] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Utech_Assets] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Utech_Assets] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Utech_Assets] SET ARITHABORT OFF 
GO
ALTER DATABASE [Utech_Assets] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Utech_Assets] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Utech_Assets] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Utech_Assets] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Utech_Assets] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Utech_Assets] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Utech_Assets] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Utech_Assets] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Utech_Assets] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Utech_Assets] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Utech_Assets] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Utech_Assets] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Utech_Assets] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Utech_Assets] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Utech_Assets] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Utech_Assets] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Utech_Assets] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Utech_Assets] SET RECOVERY FULL 
GO
ALTER DATABASE [Utech_Assets] SET  MULTI_USER 
GO
ALTER DATABASE [Utech_Assets] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Utech_Assets] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Utech_Assets] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Utech_Assets] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Utech_Assets] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Utech_Assets', N'ON'
GO
USE [Utech_Assets]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Agreement]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agreement](
	[AgreementId] [int] IDENTITY(1,1) NOT NULL,
	[AgreementName] [nvarchar](80) NULL,
 CONSTRAINT [PK_dbo.Agreement] PRIMARY KEY CLUSTERED 
(
	[AgreementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AgreementType]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AgreementType](
	[AgreementTypeId] [int] IDENTITY(1,1) NOT NULL,
	[AgreementName] [nvarchar](80) NULL,
	[AgreementCode] [nvarchar](30) NULL,
 CONSTRAINT [PK_dbo.AgreementType] PRIMARY KEY CLUSTERED 
(
	[AgreementTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
	[ApplicationUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
	[ApplicationUser_Id] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[StaffId] [int] NULL,
	[Email] [nvarchar](256) NOT NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Asset]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asset](
	[AssetId] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDatetime] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[AssetModelId] [int] NULL,
	[DateDisposedOf] [datetime] NULL,
	[DatePurchased] [datetime] NULL,
	[AssignedRoomId] [int] NULL,
	[AssetLifecycleStagesId] [int] NULL,
	[DesignatedStaffId] [int] NULL,
	[LedgerCode] [nchar](10) NULL,
	[CostPrice] [decimal](18, 0) NULL,
	[TotalTax] [decimal](18, 0) NULL,
	[UtechCode] [nvarchar](256) NULL,
	[OwnerSchoolId] [int] NULL,
	[EstYearsToDepreciate] [int] NULL,
	[AgreementId] [int] NULL,
	[OtherDetails] [nvarchar](300) NULL,
	[CurrentValue] [decimal](18, 0) NULL,
	[AgreementTypeId] [int] NULL,
	[TransactionId] [int] NULL,
	[SerialNumber] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.Asset] PRIMARY KEY CLUSTERED 
(
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Asset_Transaction]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asset_Transaction](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[TransactionDate] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[TransactionDetails] [nchar](300) NULL,
	[TotalItems] [int] NULL,
	[TotalCost] [decimal](18, 0) NULL,
	[ChequeNumber] [nvarchar](128) NULL,
	[SupplierId] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[IsCompleted] [bit] NULL,
 CONSTRAINT [PK_dbo.Asset_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetDepreciation]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetDepreciation](
	[AssetDepreciationId] [int] NOT NULL,
	[AssetId] [int] NULL,
	[DepreciationMethodId] [int] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[AssetCost] [decimal](18, 0) NULL,
	[PeriodStartDatetime] [datetime] NULL,
	[PeriodEndDatetime] [datetime] NULL,
	[NumberOfYears] [int] NULL,
	[DepreciatingValue] [decimal](18, 0) NULL,
	[DepreciationTypeId] [int] NULL,
	[ResidualValue] [decimal](18, 0) NULL,
 CONSTRAINT [PK_dbo.AssetDepreciation] PRIMARY KEY CLUSTERED 
(
	[AssetDepreciationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetLifecycleStage]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetLifecycleStage](
	[AssetLifeCycleStageId] [int] IDENTITY(1,1) NOT NULL,
	[LifecycleStageId] [int] NULL,
	[Desc] [nvarchar](500) NULL,
	[LifecycleStageName] [nvarchar](128) NULL,
	[LifeCycleStageCode] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.AssetLifecycleStage] PRIMARY KEY CLUSTERED 
(
	[AssetLifeCycleStageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetLocation]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetLocation](
	[AssetLocationId] [int] IDENTITY(1,1) NOT NULL,
	[BuildingRoomId] [int] NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[AssetId] [int] NULL,
 CONSTRAINT [PK_dbo.AssetLocation] PRIMARY KEY CLUSTERED 
(
	[AssetLocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetModel]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetModel](
	[AssetModelId] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[AssetName] [nvarchar](150) NULL,
	[ProductCode] [nvarchar](128) NULL,
	[Manufacturer] [nvarchar](128) NULL,
	[DepreciationMethodId] [int] NULL,
	[ImageUrl] [nvarchar](256) NULL,
	[Desc] [nvarchar](300) NULL,
	[AssetTypeId] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_dbo.AssetModel] PRIMARY KEY CLUSTERED 
(
	[AssetModelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetType]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetType](
	[AssetTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](128) NULL,
	[Desc] [nvarchar](300) NULL,
 CONSTRAINT [PK_dbo.AssetType] PRIMARY KEY CLUSTERED 
(
	[AssetTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssetValuation]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetValuation](
	[AssetValuationId] [int] IDENTITY(1,1) NOT NULL,
	[BookValue] [decimal](18, 0) NULL,
	[DateOfValuation] [datetime] NULL,
	[AssetId] [int] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[AssocAssetDepreciationId] [int] NULL,
 CONSTRAINT [PK_dbo.AssetValuation] PRIMARY KEY CLUSTERED 
(
	[AssetValuationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Building]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Building](
	[BuildingId] [int] IDENTITY(1,1) NOT NULL,
	[BuildingName] [nvarchar](128) NULL,
	[BuildingAcronym] [nvarchar](32) NULL,
	[BuildingCode] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.Building] PRIMARY KEY CLUSTERED 
(
	[BuildingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BuildingFloor]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildingFloor](
	[BuildingFloorId] [int] IDENTITY(1,1) NOT NULL,
	[AssociatedBuildingId] [int] NULL,
	[FloorId] [int] NULL,
	[BuildingFloorCode] [nvarchar](12) NULL,
 CONSTRAINT [PK_dbo.BuildingFloor] PRIMARY KEY CLUSTERED 
(
	[BuildingFloorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BuildingRoom]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildingRoom](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomName] [nvarchar](128) NULL,
	[RoomCode] [nvarchar](128) NULL,
	[AssociatedFloorId] [int] NULL,
	[RoomTypeId] [int] NULL,
	[IsDeleted] [bit] NULL,
	[ImageUrl] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.BuildingRoom] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DepreciationMethod]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepreciationMethod](
	[DepreciationMethodId] [int] IDENTITY(1,1) NOT NULL,
	[DepreciationMethodName] [nvarchar](128) NULL,
	[Desc] [nvarchar](500) NULL,
	[DepreciationMethodCode] [nvarchar](128) NULL,
	[Formula] [nvarchar](300) NULL,
 CONSTRAINT [PK_dbo.DepreciationMethod] PRIMARY KEY CLUSTERED 
(
	[DepreciationMethodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DepreciationType]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepreciationType](
	[DepreciationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.DepreciationType] PRIMARY KEY CLUSTERED 
(
	[DepreciationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Faculty]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faculty](
	[FacultyId] [int] IDENTITY(1,1) NOT NULL,
	[FacultyName] [nvarchar](128) NULL,
	[FacultyCode] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.Faculty] PRIMARY KEY CLUSTERED 
(
	[FacultyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Floor]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Floor](
	[FloorId] [int] IDENTITY(1,1) NOT NULL,
	[FloorName] [nvarchar](128) NULL,
	[FloorCode] [nvarchar](128) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_dbo.Floor] PRIMARY KEY CLUSTERED 
(
	[FloorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LeasedAsset]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LeasedAsset](
	[LeaseId] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[DispositionCost] [decimal](18, 0) NULL,
	[MonthlyPayment] [decimal](18, 0) NULL,
	[AssetId] [int] NULL,
 CONSTRAINT [PK_dbo.LeasedAsset] PRIMARY KEY CLUSTERED 
(
	[LeaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ModificationType]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModificationType](
	[ModificationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ModificationName] [nvarchar](128) NULL,
	[Desc] [nvarchar](256) NULL,
 CONSTRAINT [PK_dbo.ModificationType] PRIMARY KEY CLUSTERED 
(
	[ModificationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Parish]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Parish](
	[ParishId] [int] IDENTITY(1,1) NOT NULL,
	[ParishName] [varchar](60) NULL,
 CONSTRAINT [PK_dbo.Parish] PRIMARY KEY CLUSTERED 
(
	[ParishId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ref_Gender]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ref_Gender](
	[GenderId] [int] IDENTITY(1,1) NOT NULL,
	[GenderName] [nvarchar](40) NULL,
 CONSTRAINT [PK_dbo.Ref_Gender] PRIMARY KEY CLUSTERED 
(
	[GenderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ref_RoomType]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ref_RoomType](
	[RoomTypeId] [int] IDENTITY(1,1) NOT NULL,
	[RoomTypeName] [nvarchar](128) NULL,
	[Desc] [nvarchar](300) NULL,
 CONSTRAINT [PK_dbo.Ref_RoomType] PRIMARY KEY CLUSTERED 
(
	[RoomTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[School]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[School](
	[SchoolId] [int] IDENTITY(1,1) NOT NULL,
	[SchoolName] [nvarchar](128) NULL,
	[SchoolCode] [nvarchar](128) NULL,
	[AssociatedFacultyId] [int] NULL,
 CONSTRAINT [PK_dbo.School] PRIMARY KEY CLUSTERED 
(
	[SchoolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Staff]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[StaffId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](128) NULL,
	[LastName] [nvarchar](128) NULL,
	[JobDescription] [nvarchar](128) NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[GenderId] [int] NULL,
	[IsDeleted] [bit] NULL,
	[ProfileImageUrl] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[CellPhoneNum] [nvarchar](16) NOT NULL,
	[AddressLine] [nvarchar](300) NULL,
	[Country] [nvarchar](80) NULL,
 CONSTRAINT [PK_dbo.Staff] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [nvarchar](256) NULL,
	[SupplierCode] [nvarchar](50) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[Email] [nvarchar](256) NULL,
	[AddressLine] [nvarchar](300) NULL,
	[Country] [nvarchar](80) NULL,
	[TelephoneNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_dbo.Supplier] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SupplierAddress]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierAddress](
	[SupplierAddressId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierId] [int] NULL,
	[Address] [nvarchar](150) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
	[AddressLine] [nvarchar](300) NULL,
	[Country] [nvarchar](80) NULL,
 CONSTRAINT [PK_dbo.SupplierAddress] PRIMARY KEY CLUSTERED 
(
	[SupplierAddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SupplierTelephone]    Script Date: 01/12/2014 07:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierTelephone](
	[SupplierTelephoneId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierId] [int] NULL,
	[TelephoneNumber] [nvarchar](25) NULL,
	[CreatedDateTime] [datetime] NULL,
	[CreatedByUserId] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.SupplierTelephone] PRIMARY KEY CLUSTERED 
(
	[SupplierTelephoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 01/12/2014 07:55:32 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ApplicationUser_Id]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_ApplicationUser_Id] ON [dbo].[AspNetUserClaims]
(
	[ApplicationUser_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ApplicationUser_Id]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_ApplicationUser_Id] ON [dbo].[AspNetUserLogins]
(
	[ApplicationUser_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 01/12/2014 07:55:32 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssetModelId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssetModelId] ON [dbo].[Asset]
(
	[AssetModelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[Asset]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[Asset_Transaction]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SupplierId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_SupplierId] ON [dbo].[Asset_Transaction]
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssetId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssetId] ON [dbo].[AssetDepreciation]
(
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[AssetDepreciation]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepreciationMethodId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_DepreciationMethodId] ON [dbo].[AssetDepreciation]
(
	[DepreciationMethodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepreciationTypeId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_DepreciationTypeId] ON [dbo].[AssetDepreciation]
(
	[DepreciationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssetId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssetId] ON [dbo].[AssetLocation]
(
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_BuildingRoomId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_BuildingRoomId] ON [dbo].[AssetLocation]
(
	[BuildingRoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[AssetLocation]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[AssetModel]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_DepreciationMethodId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_DepreciationMethodId] ON [dbo].[AssetModel]
(
	[DepreciationMethodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssetId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssetId] ON [dbo].[AssetValuation]
(
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssocAssetDepreciationId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssocAssetDepreciationId] ON [dbo].[AssetValuation]
(
	[AssocAssetDepreciationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[AssetValuation]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssociatedBuildingId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssociatedBuildingId] ON [dbo].[BuildingFloor]
(
	[AssociatedBuildingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FloorId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_FloorId] ON [dbo].[BuildingFloor]
(
	[FloorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssociatedFloorId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssociatedFloorId] ON [dbo].[BuildingRoom]
(
	[AssociatedFloorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_RoomTypeId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_RoomTypeId] ON [dbo].[BuildingRoom]
(
	[RoomTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssetId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssetId] ON [dbo].[LeasedAsset]
(
	[AssetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssociatedFacultyId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_AssociatedFacultyId] ON [dbo].[School]
(
	[AssociatedFacultyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[Staff]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_GenderId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_GenderId] ON [dbo].[Staff]
(
	[GenderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[Supplier]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[SupplierAddress]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SupplierId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_SupplierId] ON [dbo].[SupplierAddress]
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CreatedByUserId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_CreatedByUserId] ON [dbo].[SupplierTelephone]
(
	[CreatedByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SupplierId]    Script Date: 01/12/2014 07:55:32 ******/
CREATE NONCLUSTERED INDEX [IX_SupplierId] ON [dbo].[SupplierTelephone]
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_ApplicationUser_Id] FOREIGN KEY([ApplicationUser_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_ApplicationUser_Id]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_ApplicationUser_Id] FOREIGN KEY([ApplicationUser_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_ApplicationUser_Id]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Asset_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_dbo.Asset_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Asset_dbo.AssetModel_AssetModelId] FOREIGN KEY([AssetModelId])
REFERENCES [dbo].[AssetModel] ([AssetModelId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_dbo.Asset_dbo.AssetModel_AssetModelId]
GO
ALTER TABLE [dbo].[Asset_Transaction]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Asset_Transaction_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Asset_Transaction] CHECK CONSTRAINT [FK_dbo.Asset_Transaction_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[Asset_Transaction]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Asset_Transaction_dbo.Supplier_SupplierId] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO
ALTER TABLE [dbo].[Asset_Transaction] CHECK CONSTRAINT [FK_dbo.Asset_Transaction_dbo.Supplier_SupplierId]
GO
ALTER TABLE [dbo].[AssetDepreciation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetDepreciation_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AssetDepreciation] CHECK CONSTRAINT [FK_dbo.AssetDepreciation_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[AssetDepreciation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetDepreciation_dbo.Asset_AssetId] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AssetDepreciation] CHECK CONSTRAINT [FK_dbo.AssetDepreciation_dbo.Asset_AssetId]
GO
ALTER TABLE [dbo].[AssetDepreciation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetDepreciation_dbo.DepreciationMethod_DepreciationMethodId] FOREIGN KEY([DepreciationMethodId])
REFERENCES [dbo].[DepreciationMethod] ([DepreciationMethodId])
GO
ALTER TABLE [dbo].[AssetDepreciation] CHECK CONSTRAINT [FK_dbo.AssetDepreciation_dbo.DepreciationMethod_DepreciationMethodId]
GO
ALTER TABLE [dbo].[AssetDepreciation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetDepreciation_dbo.DepreciationType_DepreciationTypeId] FOREIGN KEY([DepreciationTypeId])
REFERENCES [dbo].[DepreciationType] ([DepreciationTypeId])
GO
ALTER TABLE [dbo].[AssetDepreciation] CHECK CONSTRAINT [FK_dbo.AssetDepreciation_dbo.DepreciationType_DepreciationTypeId]
GO
ALTER TABLE [dbo].[AssetLocation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetLocation_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AssetLocation] CHECK CONSTRAINT [FK_dbo.AssetLocation_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[AssetLocation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetLocation_dbo.Asset_AssetId] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AssetLocation] CHECK CONSTRAINT [FK_dbo.AssetLocation_dbo.Asset_AssetId]
GO
ALTER TABLE [dbo].[AssetLocation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetLocation_dbo.BuildingRoom_BuildingRoomId] FOREIGN KEY([BuildingRoomId])
REFERENCES [dbo].[BuildingRoom] ([RoomId])
GO
ALTER TABLE [dbo].[AssetLocation] CHECK CONSTRAINT [FK_dbo.AssetLocation_dbo.BuildingRoom_BuildingRoomId]
GO
ALTER TABLE [dbo].[AssetModel]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetModel_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AssetModel] CHECK CONSTRAINT [FK_dbo.AssetModel_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[AssetModel]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetModel_dbo.DepreciationMethod_DepreciationMethodId] FOREIGN KEY([DepreciationMethodId])
REFERENCES [dbo].[DepreciationMethod] ([DepreciationMethodId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AssetModel] CHECK CONSTRAINT [FK_dbo.AssetModel_dbo.DepreciationMethod_DepreciationMethodId]
GO
ALTER TABLE [dbo].[AssetValuation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetValuation_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AssetValuation] CHECK CONSTRAINT [FK_dbo.AssetValuation_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[AssetValuation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetValuation_dbo.Asset_AssetId] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AssetValuation] CHECK CONSTRAINT [FK_dbo.AssetValuation_dbo.Asset_AssetId]
GO
ALTER TABLE [dbo].[AssetValuation]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssetValuation_dbo.AssetDepreciation_AssocAssetDepreciationId] FOREIGN KEY([AssocAssetDepreciationId])
REFERENCES [dbo].[AssetDepreciation] ([AssetDepreciationId])
GO
ALTER TABLE [dbo].[AssetValuation] CHECK CONSTRAINT [FK_dbo.AssetValuation_dbo.AssetDepreciation_AssocAssetDepreciationId]
GO
ALTER TABLE [dbo].[BuildingFloor]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BuildingFloor_dbo.Building_AssociatedBuildingId] FOREIGN KEY([AssociatedBuildingId])
REFERENCES [dbo].[Building] ([BuildingId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BuildingFloor] CHECK CONSTRAINT [FK_dbo.BuildingFloor_dbo.Building_AssociatedBuildingId]
GO
ALTER TABLE [dbo].[BuildingFloor]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BuildingFloor_dbo.Floor_FloorId] FOREIGN KEY([FloorId])
REFERENCES [dbo].[Floor] ([FloorId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BuildingFloor] CHECK CONSTRAINT [FK_dbo.BuildingFloor_dbo.Floor_FloorId]
GO
ALTER TABLE [dbo].[BuildingRoom]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BuildingRoom_dbo.BuildingFloor_AssociatedFloorId] FOREIGN KEY([AssociatedFloorId])
REFERENCES [dbo].[BuildingFloor] ([BuildingFloorId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BuildingRoom] CHECK CONSTRAINT [FK_dbo.BuildingRoom_dbo.BuildingFloor_AssociatedFloorId]
GO
ALTER TABLE [dbo].[BuildingRoom]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BuildingRoom_dbo.Ref_RoomType_RoomTypeId] FOREIGN KEY([RoomTypeId])
REFERENCES [dbo].[Ref_RoomType] ([RoomTypeId])
GO
ALTER TABLE [dbo].[BuildingRoom] CHECK CONSTRAINT [FK_dbo.BuildingRoom_dbo.Ref_RoomType_RoomTypeId]
GO
ALTER TABLE [dbo].[LeasedAsset]  WITH CHECK ADD  CONSTRAINT [FK_dbo.LeasedAsset_dbo.Asset_AssetId] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([AssetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LeasedAsset] CHECK CONSTRAINT [FK_dbo.LeasedAsset_dbo.Asset_AssetId]
GO
ALTER TABLE [dbo].[School]  WITH CHECK ADD  CONSTRAINT [FK_dbo.School_dbo.Faculty_AssociatedFacultyId] FOREIGN KEY([AssociatedFacultyId])
REFERENCES [dbo].[Faculty] ([FacultyId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[School] CHECK CONSTRAINT [FK_dbo.School_dbo.Faculty_AssociatedFacultyId]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Staff_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_dbo.Staff_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Staff_dbo.Ref_Gender_GenderId] FOREIGN KEY([GenderId])
REFERENCES [dbo].[Ref_Gender] ([GenderId])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_dbo.Staff_dbo.Ref_Gender_GenderId]
GO
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Supplier_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Supplier] CHECK CONSTRAINT [FK_dbo.Supplier_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[SupplierAddress]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SupplierAddress_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SupplierAddress] CHECK CONSTRAINT [FK_dbo.SupplierAddress_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[SupplierAddress]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SupplierAddress_dbo.Supplier_SupplierId] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SupplierAddress] CHECK CONSTRAINT [FK_dbo.SupplierAddress_dbo.Supplier_SupplierId]
GO
ALTER TABLE [dbo].[SupplierTelephone]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SupplierTelephone_dbo.AspNetUsers_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[SupplierTelephone] CHECK CONSTRAINT [FK_dbo.SupplierTelephone_dbo.AspNetUsers_CreatedByUserId]
GO
ALTER TABLE [dbo].[SupplierTelephone]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SupplierTelephone_dbo.Supplier_SupplierId] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SupplierTelephone] CHECK CONSTRAINT [FK_dbo.SupplierTelephone_dbo.Supplier_SupplierId]
GO
USE [master]
GO
ALTER DATABASE [Utech_Assets] SET  READ_WRITE 
GO
