<?php namespace App\Repositories\Asset\AssetDepreciation;

use App\Repositories\BaseRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of AssetDepreciationRepository
 *
 * @author hue
 */
class AssetDepreciationRepository extends BaseRepository implements \App\Repositories\Asset\AssetDepreciation {

    
    
    public function __construct(\App\Repositories\Asset\AssetDepreciation $assetDepreciation, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
       
    }
}
