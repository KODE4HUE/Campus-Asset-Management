<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepreciationMethodTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('depreciation_method', function( Blueprint $table ){
               
                $table->increments('id');
                $table->string('depreciation_method_name', 128)->default('');
                $table->string('depreciation_method_code', 60)->default('');
                $table->string('description', 300)->default('');
            
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('depreciation_method');
	}

}
