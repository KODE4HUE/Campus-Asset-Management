<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Model\System\RoleManagement\Role;

class InsertDataForRoleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
                $administrator = new Role;
                $administrator->name = "Administrator";
                $administrator->save();
                
                $securityPersonnel = new Role;
                $securityPersonnel->name = "Security Personnel";
                $securityPersonnel->save();
                
                $accountsPersonnel = new Role;
                $accountsPersonnel->name = "Accounts Personnel";
                $accountsPersonnel->save();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

            DB::table('role')->delete();
	}

}
