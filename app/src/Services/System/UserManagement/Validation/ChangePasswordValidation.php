<?php namespace App\Services\System\UserManagement\Validation;

use App\Services\CustomValidation;

class ChangePasswordValidation extends CustomValidation
{
    protected static $rules = [
        'old_password'      => 'required',
        'password'           => 'required|different:old_password',
        'confirm_password'  => 'required|same:password',
       
    ];
        
}