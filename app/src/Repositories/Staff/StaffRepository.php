<?php namespace App\Repositories\Staff;

use App\Repositories\BaseRepository;
use Model\Staff\Staff;

class StaffRepository extends BaseRepository implements IStaffRepository 
{
    
    protected $staff;
    
    public function __construct(Staff $staff, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage=25)
    {
        parent::__construct($staff, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->staff = $staff;
    }
}
