<?php namespace Model\System\RoleManagement;

class Role extends \Illuminate\Database\Eloquent\Model
{
    use \Illuminate\Database\Eloquent\SoftDeletingTrait;
        
    protected $table = 'role';

}