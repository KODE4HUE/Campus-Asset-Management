<?php namespace App\Services\System\RoleManagement;

use App\Repositories\System\Role\IRoleRepository;
use App\Repositories\System\UserManagement\UserRole\IUserRoleRepository;
use App\Services\RoleManagement\Validation\AddUserToRoleValidation;
use App\Services\ServiceManager;
use Illuminate\Support\MessageBag;

/**
 * Description of RoleServiceManager
 *
 * @author hue
 */
class RoleServiceManager extends ServiceManager
{
    
    protected $roleRepository;
    protected $userRoleRepository;
    protected $createRoleValidation;
    protected $userToRoleValidation;

    public function __construct(
            IRoleRepository $roleRepository,
            IUserRoleRepository $userRoleRepository,
            RoleValidation $roleValidation,
            UserToRoleValidation $userToRoleValidation
    )
    {
        $this->roleRepository = $roleRepository;
        $this->roleValidation = $roleValidation;
        $this->userRoleRepository = $userRoleRepository;
        $this->userToRoleValidation = $userToRoleValidation;
    }
    
    public function createRole($data)
    {
        $created =false;
        
        if($this->roleValidation->passes($data))
        {
            $result = $this->roleRepository->create($data);
            
            if(!$result['error'])
            {
                $created= true;
            }
            else
            {
                $result['error-messages']['server-error'] = "Unable to create role";
                $created = false;
            }
            
        }
        else
        {
            $created =false;
            
            $result = [
                'data'  => false,
                'error' => true,
            ];

            $result['error-messages']['validation-messages'] = $this->roleValidation->getErrors();
            
        }
        
        $this->result = $result;
        
        return $created;    
    }
    
    public function getAllRoles()
    {
        $hasData = false;
        $result = $this->roleRepository->all();
        $errorMessages = new MessageBag();
        
        if($result['error'])
        {
            $result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
            if(!$result['data'])
            {
                $errorMessages->add('custom', "No role found");
                $result['error-messages']['validation-messages'] = $errorMessages;
                $hasData = false;
            }
            else
            {
                $hasData = true;
            }
        }
        
        $this->result = $result;
        
        return $hasData;
    }
    
    public function getRole($id)
    {
        $hasData = false;
       
        $result = $this->roleRepository->get($id);
        
        if(!$result['error'])
        {
            if(!$result['data'])
            {
                $result['error-messages']['validation-error'] = new MessageBag("custom", "The role was not found");
                $hasData = false;
            }
            else
            {
                $hasData= true;
            }
            
        }
        else
        {
            $hasData = false;
            $result['error-messages']['server-error'] = "There was an error trying to retrieve the role";
            
        }
        
        $this->result = $result;
        
        return $hasData;
    }
    
    public function updateRole($data)
    {
        
        $updated = false;
        $validationMessages = new MessageBag();
        
        if($this->roleValidation->passes())
        {
            $result = $this->roleRepository->update($data);
        
            if(!$result['error'])
            {
                $updated = true;
                $result['success-message'] = "Successfully updated resource";
            }
            else
            {
                $updated = false;
                $result['error-messages']['server-error'] = "Unable to update role"; 
            }
        }
        else
        {
            
            $updated =false;
            
            $validationMessages = $this->roleValidation->getErrors();
            
            $result = [
                'data' => false,
                'error' => true
            ];
            
            $result['error-messsages']['validation-messages'] = $this->roleValidation->getErrors;
            
        }
        
        $this->result = $result;
        
        return $updated;
    }
    
    public function archiveRoleById($id)
    {
        $deleted = false;
        
        $result = $this->roleRepository->archive($id);
        
        if(!$result['error'])
        {
            $deleted= true;
        }

        $this->result = $result;
       
        return $deleted;
    }
    
    public function addUserToRole($userId, $roleId)
    {
        $created = false;
        $validationPassed = true;
        $validationErrorMessages = new MessageBag();
        
        $data = [
          'role_id' => $roleId,
          'user_id' => $userId
        ];
        
        if(!$this->userToRoleValidation->passes($data))
        {
            $result = $this->userRoleRepository->create($data);
            
            $validationPassed = false;
            $validationErrorMessages = $this->permissionWithRoleValidation->getErrors();
            
        }
        
        if($this->userRoleRepository->existsWhere($data)){
            $validationPassed = false;
            $validationErrorMessages->add('custom', 'The user selected is already assigned to the role specified.');
             
        }
        
        if($validationPassed)
        {
            $result = $this->userRoleRepository->create($data);
            
            if(!$result['error'])
            {
                $created = true;
            }
        }
        else
        {
            $result['error-messages']['validation-messages'] = $validationErrorMessages;
            $created = false;
        }
        
         $this->result = $result;
         
         return $created;
        
    }
    
    public function removeUserFromRole($userId, $roleId)
    {
        $removed = false;
        $validationPassed = true;
         $validationErrorMessages = new MessageBag();
        
        if(!$this->userRoleRepository->existsWhere($data))
        {
            $validationPassed = false;
            $validationErrorMessages->add('custom', 'The user selected is not assigned to the role specified.');
             
        }
        
        if($validationPassed)
        {
             $result = $this->userRoleRepository->delete($data);
            
            if(!$result['error'])
            {
                $created = true;
            }
        }
        else
        {
            $result['error-messages']['validation-messages'] = $validationErrorMessages;
            $created = false;
        }
        
        
        $this->result = $result;
        
        return $removed;
                
    }
    
//     public function archiveUserFromRole($userId, $roleId)
//    {
//        $removed = false;
//        $validationPassed = true;
//         $validationErrorMessages = new MessageBag();
//        
//        if(!$this->userRoleRepository->existsWhere($data))
//        {
//            $validationPassed = false;
//            $validationErrorMessages->add('custom', 'The user selected is not assigned to the role specified.');
//             
//        }
//        
//        if($validationPassed)
//        {
//             $result = $this->userRoleRepository->archive($data);
//            
//            if(!$result['error'])
//            {
//                $created = true;
//            }
//        }
//        else
//        {
//            $result['error-messages']['validation-messages'] = $validationErrorMessages;
//            $created = false;
//        }
//        
//        
//        $this->result = $result;
//        
//        return $removed;
//                
//    }
//    
}
