<?php namespace App\Services\System\UserManagement;

use App\Repositories\Staff\IStaffRepository;
use App\Repositories\System\Authentication\IAuthenticationRepository;
use App\Repositories\System\UserManagement\User\IUserRepository;
use App\Services\ServiceManager;
use App\Services\System\UserManagement\Validation\GenerateAuthTokenValidation;
use App\Services\System\UserManagement\Validation\ChangePasswordValidation;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;


/**
 * Authentication Mangager that enables the generation
 * of authentication token for users logging in to application
 *
 * @author hue
 */
class AuthenticationServiceManager extends ServiceManager
{

    protected $generateAuthTokenValidation;
    protected $resetPasswordValidation;
    protected $authenticationRepository;
    protected $userRepository;
    protected $staffRepository;


    public function __construct(
            IAuthenticationRepository $authenticationRepository,
            IUserRepository $userRepository,
            IStaffRepository $staffRepository,
            GenerateAuthTokenValidation $generateAuthTokenValidation,
            ChangePasswordValidation $changePasswordValidation
    )
    {
        $this->generateAuthTokenValidation = $generateAuthTokenValidation;
        $this->changePasswordValidation = $changePasswordValidation;
        $this->authenticationRepository = $authenticationRepository;
        $this->userRepository  = $userRepository; 
        $this->staffRepository = $staffRepository;
    }

    public function generateAuthenticationToken($credentials)
    {
        $generated = false;
        
        if($this->generateAuthTokenValidation->passes($credentials))
        {
            
            
            
            $result = $this->authenticationRepository->generateAuthenticationToken($credentials);
            
            if(!$result['error'])
            {
                $generated = true;
                $result['status-code'] = 200;
                
                 // fetch the logged in user
                $user = $this->authenticationRepository->getCurrentUser();
                
                
                /*
                 * sets first-login value to true if the user is logging in
                 * for the first time
                */                
                if($user['first-login'])
                {
                    $result['data']['first-login'] = true;
                    
                    /*
                     *  then update user and set first-login to false in user db
                     *  to prevent code from reaching here unless user is 
                     *  resetting passord again
                     */
                    $user['first_login'] = false;
                                        
                    $userUpdateDbResult = $this->userRepository->update($user);
                    
                    if($result['error'])
                    {
                        throw new Exception("there was a problem updating the current user to set first_login to false");
                    }
                    
                    $result['error'] = false;

                }
                else
                {
                    $result['data']['first-login'] = false; // set first-login to false 
                }
               
                
            }
            else
            {
              
              $generated = false;
              $result['status-code'] = 500;
            
            }
            
           
        }
        else
        {
            $generated =false;
            
            $result = [
                'data' => false,
                'error' => true,
                'status-code' => 401,
                'error-messages' => $this->generateAuthTokenValidation->getErrors()
                    
            ];
            
            
        }
        
        $this->result = $result;
        
        return $generated;
    }
    
    /**
     * Change the password of the currently logged-in/active user
     * 
     */
    public function changeUserPassword($credentials)
    {
         
        $passwordChanged = false;
        $this->resetResult();
        $validationPassed = true;
        $validationErrorMessages = new MessageBag();
        
        try
        {
            
            $this->staffRepository->beginTransaction();
            
            if(!$this->changePasswordValidation->passes($credentials))
            {
                $validationPassed = false;
                $validationErrorMessages = $this->changePasswordValidation->getErrors();            
            }
            
             //get logged in user
            $currentUserResult = $this->authenticationRepository->getLoggedInUser();

            if($currentUserResult['error'])
            {
                throw new \Exception('Could not retrieve logged in user');
            }
            
            $loggedInUser = $currentUserResult['data'];
            
            if(!Hash::check($credentials['old_password'], $loggedInUser['password']))
            {
                $validationPassed = false;
                $validationErrorMessages->add('old_password', 'The password specified doesnt match the existing one');
            }
            
            unset($credentials['confirm-password']);
            unset($credentials['old-password']);
            
           /****** Check if validation passed ******/
            if($validationPassed)
            {
               
                
                $newPassword = Hash::make($credentials['password']);
                
                $params = [
                    'first_login'   =>  0,  //sets first login to false in the event it is the user's first change password
                    'password'      =>  $newPassword,
                    'id'            =>  $loggedInUser['id']
                ];
               
                
                //Validation passed - update the user's password
                //$loggedInUser['password'] = $newPassword;

                //unset dates from fields to update
                
                //update the user's password
                $updatedUserResult = $this->userRepository->update($params);
                
                if($updatedUserResult['error'])
                {
                    throw new \Exception("There was an error updating the user's password");
                }
                
                // fetch staff information for the user logged in
                $loggedInStaffResult = $this->staffRepository->get($loggedInUser['staff_id']);
                
                if($loggedInStaffResult['error'])
                {
                    throw new \Exception("There was an error while fetching the logged in user's staff data");
                }
                
                $loggedInStaffData = $loggedInStaffResult['data'];
                
                if(!$loggedInStaffData)
                {
                    throw new \Exception("No staff data found for the logged user");
                }
                
                // then send a email that the user has successfully reset their password
                Mail::send(
                        'emails.auth.change-password',        
                        ['link' => 'ucams/login', 'password' => $credentials['password'] , 'staff_number' => $loggedInUser['username'], 'full_name' => $loggedInStaffData['first_name'] . ' ' . $loggedInStaffData['last_name']],  
                function($message) use ($loggedInStaffData){
                    $message->to($loggedInStaffData['email']);
                } );
                
                /*
                * Commit transaction
                */
                $this->staffRepository->commitTransaction();
                 $this->result['data']['sucess-message'] = "Password changed successfully";
                $passwordChanged = true;
                
            }
            else
            {
                /*
                    * Rollback transaction if a transaction was started
                    */
                   if($this->staffRepository->getTransactionLevel() > 0)
                   {
                       $this->staffRepository->rollbackTransaction();
                   }
                
                // Validation failed - set error messages for validation
                
                $this->result['data'] = false;
                $this->result['error-messages']['validation-messages'] = $validationErrorMessages;
                $this->result['error'] = true;
                $passwordChanged = false;
            }
                
        } catch (Exception $e) 
        {
            
            /*
             * Rollback transaction if a transaction was started
             */
            if($this->staffRepository->getTransactionLevel() > 0)
            {
                $this->staffRepository->rollbackTransaction();
            }
            
            dd($e->getMessage());
            
            Log::error('There was a service error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $this->result['data'] = false;
            $this->result['error-messages']['server-error'] = "Unable to reset password";
            $this->result['error'] = true;
            $reset = false;
        }
       
        return $passwordChanged;
    }
    
    public function getCurrentlyLoggedInUser()
    {
//        $hasCurrentStaff = false;
        $this->resetResult();
        
        $currentlyLoggedInUserResult = $this->authenticationRepository->getLoggedInUser();
            
        if($currentlyLoggedInUserResult['error'])
        {
            // there was an error fetching the logged in user
            
            $this->result['data'] = false;
            $this->result['error'] = true;
            $this->result['error-messages']['server-error'] = "The currently logged in user could not be retrieved";
            $hasCurrentStaff = false;
        }
        else
        {
            if($currentlyLoggedInUserResult['data'])
            {
                
                $this->result['data'] = $currentlyLoggedInUserResult['data'];
                $this->result['success-message'] = "successfully retrieved logged in user information";
                
            }
            else
            {
                $this->result['data'] = false;
                $this->result['error'] = true;
                $this->result['error-messages']['not-found'] = "the logged in user was not found";
            }
        }
        
    }
    
}
