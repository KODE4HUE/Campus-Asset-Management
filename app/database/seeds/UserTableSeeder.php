<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Model\System\RoleManagement\Role;
use Model\System\User;
use Rhumsaa\Uuid\Uuid;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        
        $email = 'hubert.graham90@gmail.com';
        
        DB::table('user')->insert([
            'id'       => Uuid::uuid4(),
            'username' => 'hue',
            'email' => $email,
            'password' => Hash::make('password'),
            'confirmed' => true
        ]);
        
        $insertedUser = DB::table('user')->where("email", "=", $email)->first();
          
        
        $adminRole = Role::where('name', '=', 'Administrator')->firstOrFail();
        $accountsRole = Role::where('name', '=', 'Accounts Personnel')->firstOrFail();
        $securityRole = Role::where('name', '=', 'Security Personnel')->firstOrFail();
        
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i s');
        
        $assignAdminRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $adminRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
        
         $assignAccountsRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $accountsRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
         
        $assignSecurityRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $securityRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
        
        // assign roles
        DB::table('assigned_role')->insert($assignAdminRoleData);
        DB::table('assigned_role')->insert($assignSecurityRoleData);
        DB::table('assigned_role')->insert($assignAccountsRoleData);
        
    }

}
