<?php namespace App\Repositories\System\UserManagement\User;

use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Model\System\User;
use Rhumsaa\Uuid\Console\Exception;
use Rhumsaa\Uuid\Uuid;

class UserRepository extends BaseRepository implements IUserRepository
{
    
    protected $user;
    
    public function __construct(User $user, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage=25)
    {
        parent::__construct($user, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->user = $user;
    }
    
    public function create(array $data) 
    {
        $result['data'] = false;
        $result['server-error'] = false;
        
        try
        {
            
            /*
             * Set unique identifier for user
             */
            $data['id'] = Uuid::uuid4();
            
            
            /*
             * Insert user into database
             */
            $insertedUser = DB::table('user')->insert($data);
            
                        
            //fetch inserted user with id
            
            $insertedUserResult = DB::table('user')->where('username', '=', $data['username'])->first();
            
            $result['error'] = false;
            $result['data'] = [
              'success-message' => 'Successully created resource',
              'id'      => $insertedUserResult['id']
            ];
           
            
        } catch (Exception $e) 
        {
              /*
             *  log error message for developers to view in log file
             */
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . ' method');
            Log::debug($e->getTraceAsString());
            
            $result['error'] = true;
            $result['data']  = false;
            $result['error-messages'] =  new MessageBag(['server-error'  => 'There was an error trying to create the specified resource']);
        }
        
        return $result;
    }
    
    public function update(array $data){
        $result['data'] = false;
        $result['error'] = false;
        
        try 
        {
            
            $updateUserQuery = DB::table('user')->where('id', $data['id']);
            
            unset($data['id']);
            
            $updateUserDbResult = $updateUserQuery->update($data);
            
            if(!$updateUserDbResult)
            {
                throw new Exception('Unable to update resource from via repo update');
            }
            
            $result['data'] = false;
            $result['error'] = false;
            $result['success-message'] =  "successfully updated resource";

        } 
        catch (Exception $e) 
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag([ 'server-error' => "There was an error trying to update the specified resource"]);
        }
        
        return $result;
    }
    
//     public function update(array $data)
//    {
//
//        $result['data'] = false;
//        $result['error'] = false;
//
//        try
//        {
//            
//            $this->model = $this->model->find($data['id']);
//            
//              // unset id from data to be updated in the record since id is already fetched in eloquent model
//            unset($data['id']);
//            
//            
//            if($this->model === null)
//            {
//                
//                throw new Exception("The record in table: ". $this->model->getTable()." with an id: ". $data["id"] ." was not found for updating");
//            }
//            
//          
//
//            foreach ($data as $key => $value)
//            {
//                $this->model->$key = $value;
//                
//            }
//            
////            dd($this->model);
//            
//            if(!$this->model->save())
//            {
//                throw new Exception('Unable to update resource from via repo update');
//            }
//            
//            $result['data'] = false;
//            $result['error'] = false;
//            $result['success-message'] =  "successfully updated resource";
//            
//        } catch (Exception $e)
//        {
//
//           dd($e->getMessage());
//            
//
//            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
//            Log::debug($e->getTraceAsString());
//
//            $result['error'] = true;
//            $result['error-messages'] = new MessageBag([ 'server-error' => "There was an error trying to update the specified resource"]);
//        }
//        
//        return $result;
//    }
    
}
