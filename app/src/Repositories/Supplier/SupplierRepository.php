<?php  namespace App\Repositories\Supplier;

use App\Repositories\BaseRepository;
use App\Repositories\Supplier\ISupplierInterface;
use Model\Supplier\Supplier;


class SupplierRepository extends BaseRepository implements ISupplierInterface {
    
    public function __construct(Supplier $supplier, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
      
    }
}
