<?php namespace App\Services\System\RoleManagement\Validation;

use App\Services\CustomValidation;

/**
 * Description of AddUserToRoleValidation
 *
 * @author hubert
 */
class AddUserToRoleValidation extends CustomValidation 
{
    public static $rules = [
        'role_id'   =>  'required',
        'user_id'   =>  'required'   
    ];

}
