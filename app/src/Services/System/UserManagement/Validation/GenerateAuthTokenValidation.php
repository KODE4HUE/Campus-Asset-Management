<?php namespace App\Services\System\UserManagement\Validation;

use App\Services\CustomValidation;


/**
 * Description of generateAuthTokenValidation
 *
 * @author hue
 */
class GenerateAuthTokenValidation extends CustomValidation 
{

    protected static $rules = [
        'username'   => 'required',
//        'email'      => 'required',
        'password'   => 'required'
    ];
}
