<?php namespace App\Services;

use Validator;

/**
 * Description of CustomValidation
 *
 * @author hue
 */
class CustomValidation 
{

    protected $errors;
    
    protected $messages = [
        
    ];


    /*
     * Returns true if their was no error after validating
     * properties, otherwise returns false
     */
    public function passes(array $attributes)
    {
        $isValid =true;
        
        if($this->messagesArrayExists())
        {
             $validator = Validator::make($attributes,static::$rules, $this->messages);
        }
        else
        {
           $validator = Validator::make($attributes,static::$rules);
        }
        
        if( $validator->fails() )
        {
           $isValid = false;
           $this->errors = $validator->messages();
        }
        
        return $isValid;
    }
    
    public function getErrors()
    {
        return $this->errors;
    }
    
    
    // HELPER METHODS
    
    /*
     * Method to check if the messages array is set
     */
    public function messagesArrayExists()
    {
        $exists = false;
        
        if(property_exists($this, 'messages'))
        {
            $exists = true;
        }
        
        return $exists;
    }
}
