<?php namespace App\Repositories\System\Authentication;


interface IAuthenticationRepository 
{
    public function generateAuthenticationToken($credentials);
    public function getLoggedInUser();
}
