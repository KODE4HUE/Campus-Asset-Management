<?php namespace App\Repositories\Agreement\Agreement;

use App\Repositories\BaseRepository;
use Model\Agreement\Agreement;

/**
 * Description of AgreementRepository
 *
 * @author hue
 */
class AgreementRepository extends BaseRepository implements IAgreementTypeRepository {

    
    public function __construct(Agreement $agreement, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
