<?php namespace App\Repositories\Asset\Asset;

use App\Repositories\BaseRepository;
use Model\Asset\Asset;

/**
 * Description of AssetRepository
 *
 * @author hue
 */
class AssetRepository extends BaseRepository implements IAssetRepository 
{
    
    public function __construct(Asset $asset, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage = 25)
    {
        parent::__construct($asset, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
