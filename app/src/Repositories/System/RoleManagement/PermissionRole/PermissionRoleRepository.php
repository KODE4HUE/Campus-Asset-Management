<?php namespace App\Repositories\System\RoleManagement\PermissionRole;

use App\Repositories\BaseRepository;
use App\Repositories\System\RoleManagement\PermissionRole\IPermissionRoleRepository;
use Model\System\RoleManagement\PermissionRole;

/**
 * Description of PermissionRoleRepository
 *
 * @author hue
 */
class PermissionRoleRepository extends BaseRepository implements IPermissionRoleRepository
{

    //put your code here
    public function __construct(PermissionRole $permissionRole, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage = 25)
    {
        parent::__construct($permissionRole, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        
    }
    
}
