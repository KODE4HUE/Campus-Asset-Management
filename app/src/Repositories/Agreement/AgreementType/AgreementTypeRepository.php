<?php namespace App\Model\Agreement\AgreementType;
use App\Repositories\BaseRepository;
use Model\Agreement\AgreementType;
use App\Repositories\Agreement\AgreementType\IAgreementTypeRepository;

/**
 * Description of AgreementTypeRepository
 *
 * @author hue
 */
class AgreementTypeRepository extends BaseRepository implements IAgreementTypeRepository
{
    
    public function __construct(AgreementType $agreementType, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
