<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertDataForRefRoomTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Computer Lab',
                'description'       =>      '',
            ]);
            
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Conference room',
                'description'       =>      '',
            ]);
            
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Drawing room',
                'description'       =>      '',
            ]);
           
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Kitchen',
                'description'       =>      '',
            ]);
            
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Lecturer theatre',
                'description'       =>      '',
            ]);
            
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Office',
                'description'       =>      '',
            ]);
            
            DB::table('ref_room_type')->insert([
               'room_type_name'     =>      'Tutorial room',
                'description'       =>      '',
            ]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            DB::table('ref_room_type')->delete();
	}

}
