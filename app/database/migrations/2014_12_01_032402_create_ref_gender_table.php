<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefGenderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// create reference table for genders  as ref_genders
            Schema::create('ref_gender', function(Blueprint $table){
                
                $table->increments('id');
                $table->string('gender_name', 8)->default('');
                
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//drop ref_gender table
            Schema::drop('ref_gender');
	}

}
