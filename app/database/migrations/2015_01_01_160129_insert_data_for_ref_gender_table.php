<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertDataForRefGenderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            DB::table('ref_gender')->insert([
               'gender_name'    =>  'male',
            ]);
            
            DB::table('ref_gender')->insert([
               'gender_name'    =>  'female',
            ]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            DB::table('ref_gender')->delete();
	}

}
