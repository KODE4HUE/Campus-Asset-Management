<?php namespace Model\System\RoleManagement;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
     use \Illuminate\Database\Eloquent\SoftDeletingTrait;

     protected $dates = ['deleted_at'];
     
     protected $table = 'permission';
        
     protected $fillable = [
         'display_name',
         'name'
     ];

}