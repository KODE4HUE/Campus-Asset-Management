<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Description of ApplicationServiceProvider
 *
 * @author hue
 */
class ApplicationServiceProvider extends ServiceProvider 
{

    //put your code here
    public function register()
    {    
        
        $this->app->bind(
                'App\Repositories\System\Authentication\IAuthenticationRepository', 
                'App\Repositories\System\Authentication\AuthenticationRepository'
        );
        
        $this->app->bind(
                'App\Repositories\System\UserManagement\User\IUserRepository', 
                'App\Repositories\System\UserManagement\User\UserRepository'
        );
        
        $this->app->bind(
                'App\Repositories\Staff\IStaffRepository', 
                'App\Repositories\Staff\StaffRepository'
        );
        
        $this->app->bind(
                'App\Repositories\System\UserManagement\User\IUserRepository', 
                'App\Repositories\System\UserManagement\User\UserRepository'
        );
        
        $this->app->bind(
                'App\Repositories\System\RoleManagement\Role\IRoleRepository', 
                'App\Repositories\System\RoleManagement\Role\RoleRepository'
        );

        $this->app->bind(
                'App\Repositories\System\UserManagement\UserRole\IUserRoleRepository', 
                'App\Repositories\System\UserManagement\UserRole\UserRoleRepository'
        );
        
        $this->app->bind(
                'App\Repositories\System\RoleManagement\Permission\IPermissionRepository', 
                'App\Repositories\System\RoleManagement\Permission\PermissionRepository'
        );
        
        $this->app->bind(
                'App\Repositories\System\RoleManagement\PermissionRole\IPermissionRoleRepository', 
                'App\Repositories\System\RoleManagement\PermissionRole\PermissionRoleRepository'
        );

        $this->app->bind(
            'App\Repositories\Agreement\IAgreementRepository',
            'App\Repositories\Agreement\AgreementRepository'
        );

        $this->app->bind(
            'App\Repositories\Agreement\IAgreementTypeRepository',
            'App\Repositories\Agreement\AgreementTypeRepository'
        );

        $this->app->bind(
            'App\Repositories\Asset\IAssetRepository',
            'App\Repositories\Asset\AssetRepository'
        );

        $this->app->bind(
            'App\Repositories\Asset\IAssetTypeRepository',
            'App\Repositories\Asset\AssetTypeRepository'
        );

        $this->app->bind(
            'App\Repositories\Asset\AssetModel\IAssetModelRepository',
            'App\Repositories\Asset\AssetModel\AssetModelRepository'
        );

        $this->app->bind(
            'App\Repositories\Asset\AssetModel\IAssetValuationRepository',
            'App\Repositories\Asset\AssetModel\AssetValuationRepository'
        );

        $this->app->bind(
            'App\Repositories\Asset\AssetModel\IAssetDepreciationRepository',
            'App\Repositories\Asset\AssetModel\AssetDepreciationRepository'
        );

        $this->app->bind(
            'App\Repositories\Asset\AssetModel\ILeasedAssetRepository',
            'App\Repositories\Asset\AssetModel\LeasedAssetRepository'
        );
        
    }
}
