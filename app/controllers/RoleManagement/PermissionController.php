<?php

use App\Services\System\RoleManagement\PermissionServiceManager;

class PermissionController extends Controller
{
    
     protected static $restful = true;
     protected $permissionServiceManager;


    public function __construct(\App\Services\System\RoleManagement\PermissionServiceManager $permissionServiceManager)
    {
        $this->permissionServiceManager = $permissionServiceManager;
    }
    
    public function getAllPermissions()
    {
        $statusCode = 200;
        
        $this->permissionServiceManager->getAllPermissions();
        
        $result = $this->permissionServiceManager->getResult();
        
        
        if(!$result['error'])
        {
            
            $statusCode = 200;
        }
        else
        {
            
            if(!$result['error']['not-found'])
            {
                $statusCode = 404;
            }
            
            if($result['error']['validation-messages'])
            {
                $statusCode = 401;
            }
            
            if($result['error']['server-error'])
            {
                $statusCode = 500;
            }
        }
      
        
        return Response::json($this->permissionServiceManager->getResult(), $statusCode);
        
    }
    
    public function getPermission($id)
    {
        
        $statusCode = 200; 
        
        $this->permissionServiceManager->getPermission($id);
        
        $result = $this->permissionServiceManager->getResult();
        
        if( !$result['error'] )
        {
            $statusCode =200;
        }
        else
        {
          
            
            if(!$result['error']['not-found'])
            {
                $statusCode = 404;
            }
            
            if($result['error']['validation-messages'])
            {
                $statusCode = 401;
            }
            
            if($result['error']['server-error'])
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($this->permissionServiceManager->getResult(), $statusCode);
    }
    
    public function createPermission()
    {
        
       
        $input = Input::only('display_name');
        
        $this->permissionServiceManager->createPermission($input);
        
        $result = $this->permissionServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            
            
            if($result['error-messages']['server-error'])
            {
                 $statusCode = 500;
            }
           
        }
        
        return Response::json($this->permissionServiceManager->getResult(), $statusCode);
    }
    
    public function updatePermission($permissionId)
    {
        $statusCode = 200; 
        $input = Input::only('display_name');
        $input['id'] = $permissionId;
        
        
        if($this->permissionServiceManager->updatePermission($input))
        {
            
            $statusCode = 200;
        }
        else
        {
            $statusCode = 401;
        }
        
        return Response::json($this->roleServiceManager->getResult(), $statusCode);
    }
    
    public function archivePermission($permissionId)
    {
        $statusCode = 200;
        $input['id'] = $permissionId;

        
        $this->permissionServiceManager->archivePermissionById($permissionId);
        
        $result = $this->permissionServiceManager->getResult();
        
        if(!$result["error"])
        {
             $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 401;
            }
            
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            
            if($result['error-messages']['server-error'])
            {
                $statusCode = 500;
            }
        }
        
        
        return Response::json($$result, $statusCode);
        
    }
    
    public function associatePermissionWithRole($roleId, $permissionId)
    {

        $this->permissionServiceManager->associatePermissionWithRole($roleId, $permissionId);
    }
}