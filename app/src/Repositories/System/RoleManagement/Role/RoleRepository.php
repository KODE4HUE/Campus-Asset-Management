<?php namespace App\Repositories\System\RoleManagement\Role;

use App\Repositories\BaseRepository;
use Exception;
use Illuminate\Support\Facades\Log;
use Model\System\Permission;
use Model\System\Role;

class RoleRepository extends BaseRepository implements IRoleRepository
{
    protected $role;


    public function __construct($model, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
    
    public function create(array $data)
    {
        $result['data'] = false;
        $result['error'] = false;
      
        $role = new Role;
        
        try
        {
            $role->fill($data);
            $role->save();
            
            $result['error'] = false;
            $result['data'] = [
              'message' => 'Successully created resource',
              'id'      => $role->id
            ];
            
        } catch (Exception $e) 
        {
             // log exception
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $result['error'] = true;
            $result['error-messages'] =  new MessageBag(['server-error'  => 'There was an error trying to retrieve the specified resource']);
           
        }
        
         return $result;
        
    }
    
}
