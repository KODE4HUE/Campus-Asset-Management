<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('supplier', function(Blueprint $table){
                
                $table->char('id', 36)->primary();
                $table->string('supplier_name', 128)->default('');
                $table->string('supplier_code', 50)->default('');
                $table->string('email',255)->default('');
                $table->string('address_line', 255)->default('');
                $table->string('country', 80)->default('');
                $table->string('telephone_number', 20)->default('');
                    
            });
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

            Schema::drop('supplier');
	}

}
