<?php namespace App\Services\Staff\Validation;

use App\Services\CustomValidation;
use Illuminate\Support\MessageBag;
use Validator;

class CreateStaffValidation extends CustomValidation 
{
        //put your code her
        static  protected $rules = [
            'staff_number'      => "required|unique:user,username",
            'first_name'        => "required",
            'last_name'         => "required",
            'email'             => "required|email|unique:staff,email",
            'gender_id'         => "required",
            'cell_phone_number' => "required",
            'job_description'   => "",
            'address_line'      =>  ""
        ];
        
        static protected $messages = [
            'staff_number.unique'  => 'The Staff Id has already been taken',
            'staff_number.required'=> 'The Staff Id is requireds'
        ];

        protected $isUser = false;
        
        /*
         * Returns true if the data for the staff (may include user information)
         * is valid
         * @params
         */
        public function passes(array $attributes, $hasRole = false)
        {
            
            $isValid =true;
            $errorMessages = new MessageBag();

            $validator = Validator::make($attributes,static::$rules, static::$messages);

            if( $validator->fails() )
            {
               $isValid = false;
               $errorMessages = $validator->messages();
            }
            
            // validation for staff roles
            if($hasRole)
            {
                $roles = $attributes['role_id'];
                
                //validate the role_id array
                foreach($roles as  $key => $value)
                {
                    if(isset($roles[$key]))
                    {
                        if(empty($roles[$key]))
                        {
                            $isValid = false;
                            $errorMessages->add("role_id[$key]", "Please select a role");
                        }
                        else
                        {
                            if(!is_numeric($roles[$key]))
                            {
                                $isValid = false;
                                $errorMessages->add("role_id[$key]", "Invalid role selected");
                            }
                        }
                        
                    }
                    else
                    {
                        $isValid = false;
                        $errorMessages->add("role_id[$key]", "Please select a role");
                    }
                }
            }
            
            $this->errors = $errorMessages;
            return $isValid;
        }
        
        /*
         * Sets the isUser property to true so that the adding of the user
         * specific validation can be added to the rules array when validating
         */
        public function setIsUser($value = true)
        {
            $this->isUser = $value;
        }
       
        public function resetIsUser()
        {
            $this->isUser = false;
        }
        
        
}
