<?php namespace Model\System;
//use Illuminate\Database\Eloquent\Model;
use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;

//UserInterface, RemindableInterface
//UserTrait,RemindableTrait,CamelCaseModel,
class User extends UuidModel implements \Zizaco\Confide\ConfideUserInterface
{
    
    use \Illuminate\Database\Eloquent\SoftDeletingTrait,
//    use CamelCaseModel,
    ConfideUser;
//    HasRole; // Add this trait to your user model

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'password',
        'gender_id',
        'staff_id',
        'created_by_user_id',
        'updated_by_user_id',
        'first_login',
        'confirmed',
        'confirmation_code',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('remember_token');

    public function staff()
    {
        return $this->hasOne('Model\Staff\Staff');
    }

}
